/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.controller;

import java.io.IOException;
import javafx.collections.ObservableList;
import static wolfieballdraftkit.WBDK_PropertyType.REMOVE_PLAYER_MESSAGE;
import static wolfieballdraftkit.WBDK_PropertyType.REMOVE_TEAM_MESSAGE;
import wbdk.data.Player;
import wbdk.data.Team;
import wbdk.gui.WBDK_GUI;
import wbdk.gui.MessageDialog;
import wbdk.gui.NewPlayerDialog;
import wbdk.gui.YesNoCancelDialog;
import wbdk.gui.TeamDialog;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wbdk.data.Draft;
import wbdk.data.DraftDataManager;
import wbdk.error.ErrorHandler;
import wbdk.gui.EditPlayerDialog;
import static wolfieballdraftkit.WBDK_PropertyType.*;
/**
 *
 * @author Tyler
 */
public class DraftEditController {
    MessageDialog md;
    YesNoCancelDialog yncd;
    NewPlayerDialog npd;
    EditPlayerDialog epd;
    TeamDialog td;
    
    boolean saved;
    
    ErrorHandler errorHandler;
    
    public DraftEditController(Stage initPrimaryStage, MessageDialog initMessageDialog, YesNoCancelDialog initYesNoCancelDialog) {
        md = initMessageDialog;
        yncd = initYesNoCancelDialog;
        npd = new NewPlayerDialog(initPrimaryStage, initMessageDialog);
        td = new TeamDialog(initPrimaryStage, initMessageDialog);
        epd = new EditPlayerDialog(initPrimaryStage, initMessageDialog);
    }
    /*
    public void hangleNewDraftRequest(WBDK_GUI gui) {
        try {
            // WE MAY HAVE TO SAVE CURRENT WORK
            boolean continueToMakeNew = true;
            if (!saved) {
                // THE USER CAN OPT OUT HERE WITH A CANCEL
                continueToMakeNew = promptToSave(gui);
            }

            // IF THE USER REALLY WANTS TO MAKE A NEW COURSE
            if (continueToMakeNew) {
                // RESET THE DATA, WHICH SHOULD TRIGGER A RESET OF THE UI
                DraftDataManager dataManager = gui.getDataManager();
                dataManager.reset();
                saved = false;

                // TELL THE USER THE COURSE HAS BEEN CREATED
                md.show(gui.getPropertiesManager().getProperty(NEW_DRAFT_CREATED_MESSAGE));
            }
        } catch (IOException ioe) {
            // SOMETHING WENT WRONG, PROVIDE FEEDBACK
            errorHandler.handleNewCourseError();
        }
    }
    */
    public void handleAddPlayerRequest(ObservableList<Player> players) {
        //CourseDataManager cdm = gui.getDataManager();
        //Course course = cdm.getCourse();
        npd.showAddPlayerDialog();
        
        // DID THE USER CONFIRM?
        if (npd.wasCompleteSelected()) {
            // GET THE PLAYER
            Player player = npd.getPlayer();
            
            // AND ADD IT AS A ROW TO THE TABLE
            players.add(player);
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }
    }
    public void handleRemovePlayerRequest(WBDK_GUI gui, Player playerToRemove) {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yncd.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_PLAYER_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yncd.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) { 
            for (int i = 0;i < gui.getDataManager().getDraft().getPlayers().size();i++) {
                if (((gui.getDataManager().getDraft().getPlayers()).get(i)).equals(playerToRemove)) {
                    gui.getDataManager().getDraft().getPlayers().remove(i);
                }
            }
        }
    }
    public void handleEditPlayerRequest(WBDK_GUI gui, Player playerToEdit, Boolean wasFromPlayersList, Boolean wasFromTaxiList, Team teamSelected) {
        gui.sortPlayers("all");
        Draft draft = gui.getDataManager().getDraft();
        epd.showEditPlayerDialog(playerToEdit, draft, wasFromPlayersList, wasFromTaxiList, teamSelected);
        if (epd.getSelection() == null) {
            epd.setSelection("null");
        }
        if (epd.wasCompleteSelected()) {
            // UPDATE THE SCHEDULE ITEM
            gui.getDataManager().setDraft(epd.getDraft());
            for (int i = 0;i < gui.getDataManager().getDraft().getTeams().size();i++) {
                gui.getDataManager().getDraft().getTeams().get(i).updateInfo();
            }
        }
        else if (epd.getSelection().equals("Free Agent")) {
            gui.getDataManager().getDraft().getPlayers().add(playerToEdit);
            for (int i = 0;i < (gui.getDataManager().getDraft().getTeams().size());i++) {
                if (gui.getDataManager().getDraft().getTeams().get(i).equals(teamSelected)) {
                    gui.getDataManager().getDraft().getTeams().get(i).removeStartingPlayer(playerToEdit);
                }
            }
            for (int i = 0;i < gui.getDataManager().getDraft().getTeams().size();i++) {
                gui.getDataManager().getDraft().getTeams().get(i).updateInfo();
            }
            
        }
        else {
            // THE USER MUST HAVE PRESSED CANCEL, SO
            // WE DO NOTHING
        }       
    }
    public void handleAddTeamRequest(WBDK_GUI gui) {
        td.showAddTeamDialog();
        if (td.wasCompleteSelected()) {
            Team team = td.getTeam();
            (((gui.getDataManager()).getDraft()).getTeams()).add(team);
            
            team.addPlayer(gui.getDataManager().getDraft().getPlayers().get(0), "SS");
            team.addPlayer(gui.getDataManager().getDraft().getPlayers().get(1), "1B");
            team.addPlayer(gui.getDataManager().getDraft().getPlayers().get(2), "2B");
            team.removeStartingPlayer(gui.getDataManager().getDraft().getPlayers().get(0));
            team.removeStartingPlayer(gui.getDataManager().getDraft().getPlayers().get(1));
            team.removeStartingPlayer(gui.getDataManager().getDraft().getPlayers().get(2));
            
            team.setTotalPoints(0);
            team.updateInfo();
            
        }
        else {
            
        }
    }
    
    public void handleEditTeamRequest(WBDK_GUI gui, Team teamToEdit) {
        
        td.showEditTeamDialog(teamToEdit);
        if (td.wasCompleteSelected()) {
            Team newTeam = td.getTeam();
            newTeam.setStartingPlayers(teamToEdit.getStartingPlayers());
            newTeam.setTaxiPlayers(teamToEdit.getTaxiPlayers());
            gui.getDataManager().getDraft().getTeams().remove(teamToEdit);
            gui.getDataManager().getDraft().getTeams().add(newTeam);
        }
        else { 
            
        }
    }
    
    public void handleRemoveTeamRequest(WBDK_GUI gui, Team teamToRemove) {
        yncd.show(PropertiesManager.getPropertiesManager().getProperty(REMOVE_TEAM_MESSAGE));
        String selection = yncd.getSelection();

        
        if (selection.equals(YesNoCancelDialog.YES)) { 
            if ((teamToRemove.getStartingPlayers() != null) && (teamToRemove.getStartingPlayers().size() > 0)) {
                for (int i = 0;i < teamToRemove.getStartingPlayers().size();i++) {
                    gui.getDataManager().getDraft().getPlayers().add(teamToRemove.getStartingPlayers().get(i));
                }
            }
            if ((teamToRemove.getTaxiPlayers() != null) && (teamToRemove.getTaxiPlayers().size() > 0)) {
                for (int i = 0;i < teamToRemove.getTaxiPlayers().size();i++) {
                    gui.getDataManager().getDraft().getPlayers().add(teamToRemove.getTaxiPlayers().get(i));
                }
            }
            (((gui.getDataManager()).getDraft()).getTeams()).remove(teamToRemove);
            for (int i = 0;i < gui.getDataManager().getDraft().getTeams().size();i++) {
                gui.getDataManager().getDraft().getTeams().get(i).updateInfo();
            }
        }
    }
    /*
    private boolean promptToSave(WBDK_GUI gui) throws IOException {
        // PROMPT THE USER TO SAVE UNSAVED WORK
        yncd.show((PropertiesManager.getPropertiesManager()).getProperty(SAVE_UNSAVED_WORK_MESSAGE));
        
        // AND NOW GET THE USER'S SELECTION
        String selection = yncd.getSelection();

        // IF THE USER SAID YES, THEN SAVE BEFORE MOVING ON
        if (selection.equals(YesNoCancelDialog.YES)) {
            // SAVE THE COURSE
            DraftDataManager dataManager = gui.getDataManager();
            (gui.getDraftFileManager()).saveDraft(dataManager.getDraft());
            saved = true;
            
        } // IF THE USER SAID CANCEL, THEN WE'LL TELL WHOEVER
        // CALLED THIS THAT THE USER IS NOT INTERESTED ANYMORE
        else if (selection.equals(YesNoCancelDialog.CANCEL)) {
            return false;
        }

        // IF THE USER SAID NO, WE JUST GO ON WITHOUT SAVING
        // BUT FOR BOTH YES AND NO WE DO WHATEVER THE USER
        // HAD IN MIND IN THE FIRST PLACE
        return true;
    }
    */
}
