/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

import java.io.IOException;
import java.util.Comparator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import wbdk.data.Player;
import wbdk.data.Team;
import wbdk.data.DraftPick;
import wbdk.error.ErrorHandler;
import wbdk.file.JsonDraftFileManager;
import static wolfieballdraftkit.WBDK_StartupConstants.JSON_FILE_PATH_HITTERS;
import static wolfieballdraftkit.WBDK_StartupConstants.JSON_FILE_PATH_PITCHERS;
/**
 *
 * @author Tyler
 */
public class Draft {
    private String name;
    private ObservableList<Team> teams;
    private ObservableList<Player> players;
    private ObservableList<DraftPick> draftPicks;
    JsonDraftFileManager jsonFileManager;
    ErrorHandler eH;
    public Draft() {
        name = "draft";
        teams = FXCollections.observableArrayList();
        draftPicks = FXCollections.observableArrayList();
        jsonFileManager = new JsonDraftFileManager();
        eH = ErrorHandler.getErrorHandler();
        try {
            players = jsonFileManager.loadPlayers(JSON_FILE_PATH_HITTERS, JSON_FILE_PATH_PITCHERS);
        } catch(IOException ioe) {
            eH = ErrorHandler.getErrorHandler();
            eH.handlePropertiesFileError();
            players = FXCollections.observableArrayList();
        }
        
    }
    public String getName() {
        return name;
    }
    public ObservableList<Team> getTeams() {
        return teams;
    }
    public ObservableList<Player> getPlayers() {
        return players;
    }
    public ObservableList<DraftPick> getDraftPicks() {
        return draftPicks;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setTeams(ObservableList<Team> teams) {
        this.teams = teams;
    }
    public void setPlayers(ObservableList<Player> players) {
        this.players = players;
    }
    public void setDraftPicks(ObservableList<DraftPick> draftPicks) {
        this.draftPicks = draftPicks;
    }
    public void addTeam(Team team) {
        teams.add(team);
    }
    public void addPlayer(Player player) {
        players.add(player);
    }
    public void removeTeam(Team team) {
        teams.remove(team);
    }
    public void removePlayer(Player player) {
        players.remove(player);
    }
    public void addDraftPick(DraftPick pick) {
        boolean changed = false;
        for (int i = 0;i < draftPicks.size();i++) {
            if (draftPicks.get(i).getFirstName().equals(pick.getFirstName()) && draftPicks.get(i).getLastName().equals(pick.getLastName())) {
                
                DraftPick newDraftPick = draftPicks.get(i);
                draftPicks.remove(newDraftPick);
                newDraftPick.setContract(pick.getContract());
                draftPicks.add(newDraftPick);
                changed = true;
                break;
            }
        }
        if (!changed) {
            draftPicks.add(pick);
        }
    }
    
    public void setPoints() {
        Team[] rankings = new Team[teams.size()];
        if (teams != null && teams.size() > 1) {
            for (int i = 0;i < teams.size();i++) {
                rankings[i] = teams.get(i);
                teams.get(i).setTotalPoints(0);
            }
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < teams.size() - 1;k++) {
                    if (rankings[k].getTeamR() < rankings[k + 1].getTeamR()) {
                        Team temp = rankings[k + 1];
                        rankings[k + 1] = rankings[k];
                        rankings[k] = temp;
                    }   
                }
            }
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < rankings.length;k++) {
                    if (teams.get(i).equals(rankings[k])) {
                        teams.get(i).addTotalPoints(teams.size() - k);
                        break;
                    }
                }
            }
            
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < teams.size() - 1;k++) {
                    if (rankings[k].getTeamHR() < rankings[k + 1].getTeamHR()) {
                        Team temp = rankings[k + 1];
                        rankings[k + 1] = rankings[k];
                        rankings[k] = temp;
                    }   
                }
            }
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < rankings.length;k++) {
                    if (teams.get(i).equals(rankings[k])) {
                        teams.get(i).addTotalPoints(teams.size() - k);
                        break;
                    }
                }
            }
            
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < teams.size() - 1;k++) {
                    if (rankings[k].getTeamRBI() < rankings[k + 1].getTeamRBI()) {
                        Team temp = rankings[k + 1];
                        rankings[k + 1] = rankings[k];
                        rankings[k] = temp;
                    }   
                }
            }
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < rankings.length;k++) {
                    if (teams.get(i).equals(rankings[k])) {
                        teams.get(i).addTotalPoints(teams.size() - k);
                        break;
                    }
                }
            }
            
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < teams.size() - 1;k++) {
                    if (rankings[k].getTeamSB() < rankings[k + 1].getTeamSB()) {
                        Team temp = rankings[k + 1];
                        rankings[k + 1] = rankings[k];
                        rankings[k] = temp;
                    }   
                }
            }
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < rankings.length;k++) {
                    if (teams.get(i).equals(rankings[k])) {
                        teams.get(i).addTotalPoints(teams.size() - k);
                        break;
                    }
                }
            }
            
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < teams.size() - 1;k++) {
                    if (rankings[k].getTeamBA() < rankings[k + 1].getTeamBA()) {
                        Team temp = rankings[k + 1];
                        rankings[k + 1] = rankings[k];
                        rankings[k] = temp;
                    }   
                }
            }
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < rankings.length;k++) {
                    if (teams.get(i).equals(rankings[k])) {
                        teams.get(i).addTotalPoints(teams.size() - k);
                        break;
                    }
                }
            }
            
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < teams.size() - 1;k++) {
                    if (rankings[k].getTeamW() < rankings[k + 1].getTeamW()) {
                        Team temp = rankings[k + 1];
                        rankings[k + 1] = rankings[k];
                        rankings[k] = temp;
                    }   
                }
            }
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < rankings.length;k++) {
                    if (teams.get(i).equals(rankings[k])) {
                        teams.get(i).addTotalPoints(teams.size() - k);
                        break;
                    }
                }
            }
            
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < teams.size() - 1;k++) {
                    if (rankings[k].getTeamSV() < rankings[k + 1].getTeamSV()) {
                        Team temp = rankings[k + 1];
                        rankings[k + 1] = rankings[k];
                        rankings[k] = temp;
                    }   
                }
            }
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < rankings.length;k++) {
                    if (teams.get(i).equals(rankings[k])) {
                        teams.get(i).addTotalPoints(teams.size() - k);
                        break;
                    }
                }
            }
            
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < teams.size() - 1;k++) {
                    if (rankings[k].getTeamK() < rankings[k + 1].getTeamK()) {
                        Team temp = rankings[k + 1];
                        rankings[k + 1] = rankings[k];
                        rankings[k] = temp;
                    }   
                }
            }
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < rankings.length;k++) {
                    if (teams.get(i).equals(rankings[k])) {
                        teams.get(i).addTotalPoints(teams.size() - k);
                        break;
                    }
                }
            }
            
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < teams.size() - 1;k++) {
                    if (rankings[k].getTeamERA() < rankings[k + 1].getTeamERA()) {
                        Team temp = rankings[k + 1];
                        rankings[k + 1] = rankings[k];
                        rankings[k] = temp;
                    }   
                }
            }
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < rankings.length;k++) {
                    if (teams.get(i).equals(rankings[k])) {
                        teams.get(i).addTotalPoints(teams.size() - k);
                        break;
                    }
                }
            }
            
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < teams.size() - 1;k++) {
                    if (rankings[k].getTeamWHIP() < rankings[k + 1].getTeamWHIP()) {
                        Team temp = rankings[k + 1];
                        rankings[k + 1] = rankings[k];
                        rankings[k] = temp;
                    }   
                }
            }
            for (int i = 0;i < teams.size();i++) {
                for (int k = 0;k < rankings.length;k++) {
                    if (teams.get(i).equals(rankings[k])) {
                        teams.get(i).addTotalPoints(teams.size() - k);
                        break;
                    }
                }
            }
        }
        else if (teams != null && teams.size() == 1) {
            teams.get(0).setTotalPoints(10);
        }
        else {
            //do nothing, no teams
        }
    }
    public void calculateEstimatedValues() {
        int totalMoney = 0;
        ObservableList<Player> newPlayers = FXCollections.observableArrayList();
        ObservableList<Player> bestR = FXCollections.observableArrayList();
        ObservableList<Player> bestHR = FXCollections.observableArrayList();
        ObservableList<Player> bestRBI = FXCollections.observableArrayList();
        ObservableList<Player> bestSB = FXCollections.observableArrayList();
        ObservableList<Player> bestBA = FXCollections.observableArrayList();
        
        ObservableList<Player> bestW = FXCollections.observableArrayList();
        ObservableList<Player> bestSV = FXCollections.observableArrayList();
        ObservableList<Player> bestK = FXCollections.observableArrayList();
        ObservableList<Player> bestERA = FXCollections.observableArrayList();
        ObservableList<Player> bestWHIP = FXCollections.observableArrayList();
        
        for (int i = 0;i < teams.size();i++) {
            totalMoney = totalMoney + teams.get(i).getMoney();
        }
        for (int i = 0;i < players.size();i++) {
            if (players.get(i).getIsPitcher()) {
                bestW.add(players.get(i));
                bestSV.add(players.get(i));
                bestK.add(players.get(i));
                bestERA.add(players.get(i));
                bestWHIP.add(players.get(i));
            }
            else if (!(players.get(i).getIsPitcher())) {
                bestR.add(players.get(i));
                bestHR.add(players.get(i));
                bestRBI.add(players.get(i));
                bestSB.add(players.get(i));
                bestBA.add(players.get(i));
            }
        }
        for (int i = 0;i < bestR.size() - 1;i++) {
            for (int k = i;k < bestR.size() - 1;k++) {
                if ((bestR.get(k).getRandW()) < (bestR.get(k + 1).getRandW())) {
                    Player temp = bestR.get(k);
                    bestR.set(k, bestR.get(k + 1));
                    bestR.set(k + 1, temp);
                }
                if ((bestHR.get(k).getHRandSV()) < (bestHR.get(k + 1).getHRandSV())) {
                    Player temp = bestHR.get(k);
                    bestHR.set(k, bestHR.get(k + 1));
                    bestHR.set(k + 1, temp);
                }
                if ((bestRBI.get(k).getRBIandK()) < (bestRBI.get(k + 1).getRBIandK())) {
                    Player temp = bestRBI.get(k);
                    bestRBI.set(k, bestRBI.get(k + 1));
                    bestRBI.set(k + 1, temp);
                }
                if ((bestSB.get(k).getSBandERA()) < (bestSB.get(k + 1).getSBandERA())) {
                    Player temp = bestSB.get(k);
                    bestSB.set(k, bestSB.get(k + 1));
                    bestSB.set(k + 1, temp);
                }
                if ((bestBA.get(k).getBAandWHIP()) < (bestBA.get(k + 1).getBAandWHIP())) {
                    Player temp = bestBA.get(k);
                    bestBA.set(k, bestBA.get(k + 1));
                    bestBA.set(k + 1, temp);
                }
            }
        }
        for (int i = 0;i < bestW.size() - 1;i++) {
            for (int k = 0;k < bestW.size() - 1;k++) {
                if ((bestW.get(k).getRandW()) < (bestW.get(k + 1).getRandW())) {
                    Player temp = bestW.get(k);
                    bestW.set(k, bestW.get(k + 1));
                    bestW.set(k + 1, temp);
                }
                if ((bestSV.get(k).getHRandSV()) < (bestSV.get(k + 1).getHRandSV())) {
                    Player temp = bestSV.get(k);
                    bestSV.set(k, bestSV.get(k + 1));
                    bestSV.set(k + 1, temp);
                }
                if ((bestK.get(k).getRBIandK()) < (bestK.get(k + 1).getRBIandK())) {
                    Player temp = bestK.get(k);
                    bestK.set(k, bestK.get(k + 1));
                    bestK.set(k + 1, temp);
                }
                if ((bestERA.get(k).getSBandERA()) < (bestERA.get(k + 1).getSBandERA())) {
                    Player temp = bestERA.get(k);
                    bestERA.set(k, bestERA.get(k + 1));
                    bestERA.set(k + 1, temp);
                }
                if ((bestWHIP.get(k).getBAandWHIP()) < (bestWHIP.get(k + 1).getBAandWHIP())) {
                    Player temp = bestWHIP.get(k);
                    bestWHIP.set(k, bestWHIP.get(k + 1));
                    bestWHIP.set(k + 1, temp);
                }
            }
        }
        for (int i = 0;i < bestR.size();i++) {
            bestR.get(i).setRankRW(i + 1);
            bestHR.get(i).setRankHRSV(i + 1);
            bestRBI.get(i).setRankRBIK(i + 1);
            bestSB.get(i).setRankSBERA(i + 1);
            bestBA.get(i).setRankBAWHIP(i + 1);
        }
        for (int i = 0;i < bestW.size();i++) {
            bestW.get(i).setRankRW(i + 1);
            bestSV.get(i).setRankHRSV(i + 1);
            bestK.get(i).setRankRBIK(i + 1);
            bestERA.get(i).setRankSBERA(i + 1);
            bestWHIP.get(i).setRankBAWHIP(i + 1);
        }
        for (int i = 0;i < bestR.size();i++) {
            bestR.get(i).setEstValue( (int) ((totalMoney / (bestR.size())) * ((2 * bestR.size()) / bestR.get(i).getAverageRank())));
            newPlayers.add(bestR.get(i));
        }
        for (int i = 0;i < bestW.size();i++) {
            bestW.get(i).setEstValue( ((int) ((totalMoney / (bestW.size())) * ((bestW.size() * 2) / bestW.get(i).getAverageRank()))));
            newPlayers.add(bestW.get(i));
        }
        System.out.println(totalMoney + " " + bestR.size() + " " + bestR.get(0).getAverageRank());
        System.out.println((totalMoney / (2 * bestR.size())) + " " + ((2 * bestR.size()) / bestR.get(0).getAverageRank()));
        System.out.println("bestR - " + bestR.get(0).getEstValue());
        System.out.println("bestW - " + bestW.get(0).getEstValue());
        System.out.println("newPlayers - " + newPlayers.get(0).getEstValue());
        System.out.println("before - " + players.get(0).getEstValue());
        setPlayers(newPlayers);
        System.out.println("after - " + players.get(0).getEstValue() + "\n");
    }
}
