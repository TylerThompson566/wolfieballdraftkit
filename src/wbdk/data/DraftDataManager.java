package wbdk.data;

import java.io.IOException;
import wbdk.file.DraftFileManager;
import java.time.DayOfWeek;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import wbdk.error.ErrorHandler;
import wbdk.file.JsonDraftFileManager;
import static wolfieballdraftkit.WBDK_StartupConstants.JSON_FILE_PATH_HITTERS;
import static wolfieballdraftkit.WBDK_StartupConstants.JSON_FILE_PATH_PITCHERS;

/**
 * This class manages a Draft, which means it knows how to
 * reset one with default values and generate useful dates.
 * 
 * @author Tyler
 */
public class DraftDataManager {
    // THIS IS THE DRAFT BEING EDITED
    Draft draft;
    ErrorHandler eH = ErrorHandler.getErrorHandler();
    
    // THIS IS THE UI, WHICH MUST BE UPDATED
    // WHENEVER OUR MODEL'S DATA CHANGES
    DraftDataView view;
    ObservableList<Player> DEFAULT_PLAYER_LIST;
    ObservableList<Team> DEFAULT_TEAM_LIST;
    ObservableList<DraftPick> DEFAULT_DRAFT_PICK_LIST;
    // THIS HELPS US LOAD THINGS FOR OUR DRAFT
    DraftFileManager fileManager;
    JsonDraftFileManager jsonFileManager;
    
    // DEFAULT INITIALIZATION VALUES FOR NEW DRAFTS
    static String  DEFAULT_DRAFT_NAME = "draft";
    static String   DEFAULT_TEXT = "Unknown";
    
    public DraftDataManager(DraftDataView initView) {
        view = initView;
        draft = new Draft();
    }
    
    /**
     * Accessor method for getting the Draft that this class manages.
     */
    public Draft getDraft() {
        return draft;
    }
    public void setDraft(Draft draft) {
        this.draft = draft;
    }
    
    /**
     * Accessor method for getting the file manager, which knows how
     * to read and write draft data from/to files.
     */
    public DraftFileManager getFileManager() {
        return fileManager;
    }

    /**
     * Resets the draft to its default initialized settings, triggering
     * the UI to reflect these changes.
     */
    public void reset() {
        // CLEAR ALL THE DRAFT VALUES
        DEFAULT_TEAM_LIST = FXCollections.observableArrayList();
        DEFAULT_DRAFT_PICK_LIST = FXCollections.observableArrayList();
        try {
            DEFAULT_PLAYER_LIST = jsonFileManager.loadPlayers(JSON_FILE_PATH_HITTERS, JSON_FILE_PATH_PITCHERS);
        } catch(IOException ioe) {
            eH = ErrorHandler.getErrorHandler();
            eH.handlePropertiesFileError();
            DEFAULT_PLAYER_LIST = FXCollections.observableArrayList();
        }
        
        draft.setName(DEFAULT_DRAFT_NAME);
        draft.setPlayers(DEFAULT_PLAYER_LIST);
        draft.setTeams(DEFAULT_TEAM_LIST);
        
        // AND THEN FORCE THE UI TO RELOAD THE UPDATED DRAFT
        view.reloadDraft(draft);
    }
    
    
}
