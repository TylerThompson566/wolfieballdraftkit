/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

import wbdk.data.Draft;
import wbdk.data.Player;
import wbdk.data.Team;
import wbdk.data.DraftDataManager;
/**
 *
 * @author Tyler
 */
public class DraftPick {
    private int pickNumber;
    private String firstName;
    private String lastName;
    private String teamName;
    private String contract;
    private int salary;
    public DraftPick() {
        pickNumber = 0;
        firstName = "";
        lastName = "";
        teamName = "";
        contract = "X";
        salary = 0;
    }
    public DraftPick(int pickNumber, Player player, Team team) {
        this.pickNumber = pickNumber;
        firstName = player.getFirstName();
        lastName = player.getLastName();
        teamName = team.getName();
        contract = player.getContract();
        salary = player.getSalary();
    }
    public int getPickNumber() {
        return pickNumber;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getTeamName() {
        return teamName;
    }
    public String getContract() {
        return contract;
    }
    public int getSalary() {
        return salary;
    }
    
    public void setPickNumber(int pickNumber) {
        this.pickNumber = pickNumber;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
    public void setContract(String contract) {
        this.contract = contract;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
}
    
