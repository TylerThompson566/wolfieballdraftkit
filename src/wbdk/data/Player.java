/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

/**
 *The purpose of this class is to allow for one array list of players that includes both hitters and pitchers. 
 * both of those classes will extend Player and will set the isPitcher boolean in all constructors.
 * @author Tyler
 */
public class Player {
    boolean isPitcher; // IF TRUE, ITS A PITCHER, IF FALSE, ITS A HITTER
    private String team; //MLB team
    private String lastName; //Last Name
    private String firstName; //First Name
    private String QP; //qualified position(s)
    private String position; //position for team
    private double IP; // innings pitched
    private int HandH; //hits earned/given
    private int ABandER; //at bats and earned runs
    private int RandW; //runs and walks
    private int HRandSV; //Hits and saves
    private int RBIandK; //runners batted in and bases on balls
    private double SBandERA; //stolen bases and ERA
    private String notes; //notes: must be changeable by user
    private int YoB; //year of birth
    private String NoB; //nation of birth
    private int estValue;
    private double BAandWHIP;
    int salary;
    String contract;
    
    private int rankRW;
    private int rankHRSV;
    private int rankRBIK;
    private int rankSBERA;
    private int rankBAWHIP;
    
    public Player(){}
    public Player(
            String team, 
            String lastName, 
            String firstName, 
            String QP,  
            double IP, 
            int HandH,
            int ABandER, 
            int RandW, 
            int HRandSV, 
            int RBIandK, 
            double SBandERA, 
            String notes, 
            int YoB, 
            String NoB, 
            boolean isPitcher,
            double BAandWHIP) {
        
        this.team = team;
        this.lastName = lastName;
        this.firstName = firstName;
        this.HandH = HandH;
        this.ABandER = ABandER;
        this.RandW = RandW;
        this.HRandSV = HRandSV;
        this.RBIandK = RBIandK;
        this.SBandERA = SBandERA;
        this.notes = notes;
        this.YoB = YoB;
        this.NoB = NoB;
        estValue = 0;
        this.IP = IP;
        this.QP = QP;
        this.isPitcher = isPitcher;
        if (isPitcher) {
            this.BAandWHIP = getWHIP();
        }
        else {
            this.BAandWHIP = getBA();
        }
        contract = "X";
        salary = 0;
        position = "null";
        
        rankRW = 0;
        rankHRSV = 0;
        rankRBIK = 0;
        rankSBERA = 0;
        rankBAWHIP = 0;
    }
    public boolean getIsPitcher() {
        return isPitcher;
    }
    public void setIsPitcher(boolean isPitcher) {
        this.isPitcher = isPitcher;
    }
   
    //ACCCESSORS
    public String getTeam() {
        return team;
    }
    public String getLastName() {
        return lastName;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getQP() {
        return QP;
    }
    public double getIP() {
        return IP;
    }
    public double getHandH() {
        return HandH;
    }
    public int getABandER() {
        return ABandER;
    }
    public int getRandW() {
        return RandW;
    }
    public int getHRandSV() {
        return HRandSV;
    }
    public int getRBIandK() {
        return RBIandK;
    }
    public double getSBandERA() {
        return SBandERA;
    }
    public String getNotes() {
        return notes;
    }
    public int getYoB() {
        return YoB;
    }
    public String getNoB() {
        return NoB;
    }
    public double getBAandWHIP() {
        return BAandWHIP;
    }
    public int getSalary() {
        return salary;
    }
    public String getContract() {
        return contract;
    }
    public String getPosition() {
        return position;
    }
    public int getEstValue() {
        return estValue;
    }
    public int getRankRW(){ 
        return rankRW;
    }
    public int getRankHRSV() {
        return rankHRSV;
    }
    public int getRankRBIK() {
        return rankRBIK;
    }
    public int getRankSBERA() {
        return rankSBERA;
    }
    public int getRankBAWHIP() {
        return rankBAWHIP;
    }
    public double getAverageRank() {
        return ((rankRW + rankHRSV + rankRBIK + rankSBERA + rankBAWHIP) / 5);
    }
    //Mutator Methods
    public void setTeam(String team) {
        this.team = team;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setQP(String QP) {
        this.QP = QP;
    }
    public void setIP(double IP) {
        this.IP = IP;
    }
    public void setHandH(int HandH) {
        this.HandH = HandH;
    }
    public void setABandER(int ABandER) {
        this.ABandER = ABandER;
    }
    public void setRandW(int RandW) {
        this.RandW = RandW;
    }
    public void setHRandSV(int HandSV) {
        this.HRandSV = HandSV;
    }
    public void setRBIandK(int RBIandK) {
        this.RBIandK = RBIandK;
    }
    public void setSBandERA(double SBandERA) {
        this.SBandERA = SBandERA;
    }
    public void setNotes(String notes) {
        this.notes = notes;
    }
    public void setYoB(int YoB) {
        this.YoB = YoB;
    }
    public void setNoB(String NoB) {
        this.NoB = NoB;
    }
    public void setBAandWHIP(double BAandWHIP) {
        this.BAandWHIP = BAandWHIP;
    }
    public void setSalary(int salary) {
        this.salary = salary;
    }
    public void setContract(String contract) {
        this.contract = contract;
    }
    public void setPosition(String position) {
        this.position = position;
    }
    public void setEstValue(int estValue) {
        this.estValue = estValue;
    }
    public void setRankRW(int rankRW) {
        this.rankRW = rankRW;
    }
    public void setRankHRSV(int rankHRSV) {
        this.rankHRSV = rankHRSV;
    }
    public void setRankRBIK(int rankRBIK) {
        this.rankRBIK = rankRBIK;
    }
    public void setRankSBERA(int rankSBERA) {
        this.rankSBERA = rankSBERA;
    }
    public void setRankBAWHIP(int rankBAWHIP) {
        this.rankBAWHIP = rankBAWHIP;
    }
    //Accessor methods for calculated data
    public double getERA() {
        if (isPitcher) {
            return ((ABandER * 9) / IP); //check for order of operations
        }
        else {
            return 0;
        }
    }
    public double getBA() {
        if (ABandER == 0) {
            return 0;
        }
        else {
            return (double)Math.round((HandH / ABandER) * 1000) / 1000;
        }
    }
    public double getWHIP() {
        if (isPitcher && IP != 0) {
            return (double) Math.round(((RandW + HandH) / IP) * 100) / 100;
        }
        else {
            return 0;
        }
        
    }
}
