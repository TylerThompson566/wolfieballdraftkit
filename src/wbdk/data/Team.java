/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.data;

import java.util.Comparator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import wbdk.data.Player;
/**
 *
 * @author Tyler
 */
public class Team {
    private String name;
    private String owner;
    private int money;
    private int moneyPP;
    private String[] positions;
    private ObservableList<Player> startingPlayers;
    private Player[] startingPlayersArray;
    private ObservableList<Player> taxiPlayers;
    
    private int playersNeeded;
    private int teamR;
    private int teamHR;
    private int teamRBI;
    private int teamSB;
    private double teamBA;
    private int teamW;
    private int teamSV;
    private int teamK;
    private double teamERA;
    private double teamWHIP;
    private int totalPoints;
    public Team(){}
    public Team(String name, String owner) {
        this.name = name;
        this.owner = owner;
        //positions = new String[23];
        startingPlayers = FXCollections.observableArrayList();
        startingPlayersArray = new Player[23];
        taxiPlayers = FXCollections.observableArrayList();
        money = 260;
        moneyPP = 0;
        positions[0] = "C";
        positions[1] = "C";
        positions[2] = "1B";
        positions[3] = "CI";
        positions[4] = "3B";
        positions[5] = "2B";
        positions[6] = "MI";
        positions[7] = "SS";
        for (int i = 8;i <= 12;i++) {
            positions[i] = "OF";
        }
        for (int i = 14;i < ((this.positions).length);i++) {
            positions[i] = "P";
        }
        positions[13] = "U";
        
        playersNeeded = 23;
        teamR = 0;
        teamHR = 0;
        teamRBI = 0;
        teamSB = 0;
        teamBA = 0;
        teamW = 0;
        teamSV = 0;
        teamK = 0;
        teamERA = 0;
        teamWHIP = 0;
        totalPoints = 0;
    }
    public String getName() {
        return name;
    }
    public String getOwner() {
        return owner;
    }
    public int getMoney() {
        return money;
    }
    public int getTeamR() {
        return teamR;
    }
    public int getTeamHR() {
        return teamHR;
    }
    public int getTeamRBI() {
        return teamRBI;
    }
    public int getTeamSB() {
        return teamSB;
    }
    public double getTeamBA() {
        return (double)Math.round(teamBA * 100) / 100;
    }
    public int getTeamW() {
        return teamW;
    }
    public int getTeamSV() {
        return teamSV;
    } 
    public int getTeamK() {
        return teamK;
    }
    public double getTeamERA() {
        return (double)Math.round(teamERA * 1000) / 1000;
    }
    public double getTeamWHIP() {
        return (double)Math.round(teamWHIP * 1000) / 1000;
    }
    public double getTotalPoints() {
        return totalPoints;
    }
    public int getPlayersNeeded() {
        return playersNeeded;
    }
    public int getMoneyPP() {
        return moneyPP;
    }
    //public String[] getPositions() {
    //    return positions;
    //}
    public ObservableList<Player> getStartingPlayers() {
        return startingPlayers;
    }
    public ObservableList<Player> getTaxiPlayers() {
        return taxiPlayers;
    }
    public Player[] getStartingPlayersArray() {
        return startingPlayersArray;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    public void setOwner(String owner) {
        this.owner = owner;
    }
    public void setMoney(int money) {
        this.money = money;
        updateInfo();
    }
    public void setMoneyPP(int moneyPP) {
        this.moneyPP = moneyPP;
    }
    public void setTeamR(int teamR) {
        this.teamR = teamR;
    }
    public void setTeamHR(int teamHR) {
        this.teamHR = teamHR;
    }
    public void setTeamRBI(int teamRBI){
        this.teamRBI = teamRBI;
    }
    public void setTeamSB(int teamSB) {
        this.teamSB = teamSB;
    }
    public void setTeamBA(double teamBA) {
        this.teamBA = (double)Math.round(teamBA * 100) / 100;
    }
    public void setTeamW(int teamW) {
        this.teamW = teamW;
    }
    public void setTeamSV(int teamSV) {
        this.teamSV = teamSV;
    }
    public void setTeamK(int teamK) {
        this.teamK = teamK;
    }
    public void setTeamERA(double teamERA) {
        this.teamERA = (double)Math.round(teamERA * 1000) / 1000;
    }
    public void setTeamWHIP(double teamWHIP) {
        this.teamWHIP = (double)Math.round(teamWHIP * 1000) / 1000;
    }
    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }
    public void addTotalPoints(int points) {
        totalPoints += points;
    }
    public void setStartingPlayers(ObservableList<Player> startingPlayers) {
        this.startingPlayers = startingPlayers;
        updateInfo();
    }
    public void setTaxiPlayers(ObservableList<Player> taxiPlayers) {
        this.taxiPlayers = taxiPlayers;
        updateInfo();
    }
    public void setStartingPlayersArray(Player[] startingPlayersArray) {
        this.startingPlayersArray = startingPlayersArray;
    }
    
    public boolean addPlayer(Player player, String position) {
        if (startingPlayers == null) {
            startingPlayers = FXCollections.observableArrayList();
            if (money == 0) {
                money = 260;
                moneyPP = 0;
                playersNeeded = 23;
                teamR = 0;
                teamHR = 0;
                teamRBI = 0;
                teamSB = 0;
                teamBA = 0;
                teamW = 0;
                teamSV = 0;
                teamK = 0;
                teamERA = 0;
                teamWHIP = 0;
                totalPoints = 0;
            }
        }
        if (startingPlayersArray == null) {
            startingPlayersArray = new Player[23];
        }
        if (startingPlayers.size() < 23) {
            return addStartingPlayer(player, position);
        }
        else {
            if (taxiPlayers.size() < 8) {
                return addTaxiPlayer(player, position);
            }
        }
        return false;
    }
    
    public boolean addStartingPlayer(Player player, String position) {
        boolean added = false;
        player.setPosition(position);
        if (startingPlayers == null) {
            startingPlayers = FXCollections.observableArrayList();
            if (money == 0) {
                money = 260;
                moneyPP = 0;
                playersNeeded = 23;
                teamR = 0;
                teamHR = 0;
                teamRBI = 0;
                teamSB = 0;
                teamBA = 0;
                teamW = 0;
                teamSV = 0;
                teamK = 0;
                teamERA = 0;
                teamWHIP = 0;
                totalPoints = 0;
            }
        }
        if (taxiPlayers == null) {
            taxiPlayers = FXCollections.observableArrayList();
        }
        if (startingPlayersArray == null) {
            startingPlayersArray = new Player[23];
            for (int i = 0;i < startingPlayersArray.length;i++) {
                startingPlayersArray[i] = null;
            }
        }
        if ((money < player.getSalary()) || (player.getContract().equals("X")) || (player.getContract().equals("S1"))) {
            return false;
        }
        else {
            if (position.equals("C")) {
                if (startingPlayersArray[0] == null) {
                    startingPlayersArray[0] = player;
                    startingPlayers.add(player);
                    sortStartingPlayers();
                    added = true;
                }
                else if (startingPlayersArray[1] == null) {
                    startingPlayersArray[1] = player;
                    startingPlayers.add(player);
                    sortStartingPlayers();
                    added = true;
                }
                else {
                    return false;
                }
            }
            else if (position.equals("1B")) {
                if (startingPlayersArray[2] == null) {
                    startingPlayersArray[2] = player;
                    startingPlayers.add(player);
                    sortStartingPlayers();
                    added = true;
                }
                else {
                    return false;
                }
            }
            else if (position.equals("3B")) {
                if (startingPlayersArray[4] == null) {
                    startingPlayersArray[4] = player;
                    startingPlayers.add(player);
                    sortStartingPlayers();
                    added = true;
                }
                else {
                    return false;
                }
            }
            else if (position.equals("CI")) {
                if (startingPlayersArray[3] == null) {
                    startingPlayersArray[3] = player;
                    startingPlayers.add(player);
                    sortStartingPlayers();
                    added = true;
                }
                else {
                    return false;
                }
            }
            else if (position.equals("2B")) {
                if (startingPlayersArray[5] == null) {
                    startingPlayersArray[5] = player;
                    startingPlayers.add(player);
                    sortStartingPlayers();
                    added = true;
                }
                else {
                    return false;
                }
            }
            else if (position.equals("SS")) {
                if (startingPlayersArray[7] == null) {
                    startingPlayersArray[7] = player;
                    startingPlayers.add(player);
                    sortStartingPlayers();
                    added = true;
                }
                else {
                    return false;
                }
            }
            else if (position.equals("MI")) {
                if (startingPlayersArray[6] == null) {
                    startingPlayersArray[6] = player;
                    startingPlayers.add(player);
                    sortStartingPlayers();
                    added = true;
                }
                else {
                    return false;
                }
            }
            else if (position.equals("OF")) {
                for (int i = 8;i <= 12;i++) {
                    if (startingPlayersArray[i] == null) {
                        startingPlayersArray[i] = player;
                        startingPlayers.add(player);
                        sortStartingPlayers();
                        added = true;
                        break;
                    }
                }
                return false;
            }
            else if (position.equals("P")) {
                for (int i = 14;i <= 22;i++) {
                    if (startingPlayersArray[i] == null) {
                        startingPlayersArray[i] = player;
                        startingPlayers.add(player);
                        sortStartingPlayers();
                        added = true;
                        break;
                    }
                }
                return false;
            }
            else if (position.equals("U")) {
                if (startingPlayersArray[13] == null) {
                    startingPlayersArray[13] = player;
                    startingPlayers.add(player);
                    sortStartingPlayers();
                    added = true;
                }
                else {
                    return false;
                }
            }
        }
        if (added == true) {
            updateInfo(player, true);
                return true;
        }
        else {
            return false;
        }
    }
    public void removeStartingPlayer(Player player) {
        if (startingPlayers == null) {
            startingPlayers = FXCollections.observableArrayList();
            if (money == 0) {
                money = 260;
                moneyPP = 0;
                playersNeeded = 23;
                teamR = 0;
                teamHR = 0;
                teamRBI = 0;
                teamSB = 0;
                teamBA = 0;
                teamW = 0;
                teamSV = 0;
                teamK = 0;
                teamERA = 0;
                teamWHIP = 0;
                totalPoints = 0;
            }
            if (startingPlayersArray == null) {
                startingPlayersArray = new Player[23];
            }
            for (int i = 0;i < startingPlayersArray.length;i++) {
                if (startingPlayersArray[i] != null) {
                    startingPlayers.add(startingPlayersArray[i]);
                }
            }
        }
        startingPlayers.remove(player);
        for (int i = 0;i < startingPlayersArray.length;i++) {
            if ((startingPlayersArray[i] != null) && (startingPlayersArray[i]).equals(player)) {
                startingPlayersArray[i] = null;
                break;
            }
        }
        updateInfo(player, false);
    }
    public boolean addTaxiPlayer(Player player, String position) {
        player.setPosition(position);
        taxiPlayers.add(player);
        updateInfo(player, true);
        return true;
        /*
        player.setPosition(position);
        if (!((player.getQP()).contains(position))) {
            
        }
        else {
            if (taxiPlayers.size() >= 8) {
                
            }
            else {
                taxiPlayers.add(player);
            } 
        }
        
                */
    }
    public void removeTaxiPlayer(Player player) {
        taxiPlayers.remove(player);
        updateInfo(player, false);
    }
    
    public void sortStartingPlayers() {
        startingPlayers.sort((Player o1, Player o2) -> o1.getPosition().compareTo(o2.getPosition()));
    }
    public void updateInfo(Player player, boolean addedPlayer) {
        if (addedPlayer  == true) {
            money -= player.getSalary();
        }
        else {
            money += player.getSalary();
        }
        playersNeeded = 23 - startingPlayers.size();
        if (startingPlayers.size() == 0) {
            moneyPP = 0;
        }
        else {
            moneyPP = (int) ((260 - money) / startingPlayers.size());
        }
        if (startingPlayers != null && startingPlayers.size() > 0) {
            double totalERA = 0;
            double totalWHIP = 0;
            double totalBA = 0;
            teamW = 0;
            teamSV = 0;
            teamK = 0;
            teamR = 0;
            teamHR = 0;
            teamRBI = 0;
            teamSB = 0;
            for (int i = 0;i < startingPlayers.size();i++) {
                if (startingPlayers.get(i).getIsPitcher()) {
                    teamW += startingPlayers.get(i).getRandW();
                    teamSV += startingPlayers.get(i).getHRandSV();
                    teamK += startingPlayers.get(i).getRBIandK();
                    totalERA += startingPlayers.get(i).getSBandERA();
                    totalWHIP += startingPlayers.get(i).getBAandWHIP();
                }
                else if (!(startingPlayers.get(i).getIsPitcher())) {
                    teamR += startingPlayers.get(i).getRandW();
                    teamHR += startingPlayers.get(i).getHRandSV();
                    teamRBI += startingPlayers.get(i).getRBIandK();
                    teamSB += startingPlayers.get(i).getSBandERA();
                    teamBA += startingPlayers.get(i).getBAandWHIP();
                    totalBA += startingPlayers.get(i).getBAandWHIP();
                }
            }
            teamERA = totalERA / startingPlayers.size();
            teamWHIP = totalWHIP / startingPlayers.size();
            teamBA = totalBA / startingPlayers.size();
        }
    }
    public void updateInfo() {
        if (startingPlayers.size() == 0) {
            moneyPP = 0;
        }
        else {
            moneyPP = (int) ((260 - money) / startingPlayers.size());
        }
        if (startingPlayers != null && startingPlayers.size() > 0) {
            double totalERA = 0;
            double totalWHIP = 0;
            double totalBA = 0;
            teamW = 0;
            teamSV = 0;
            teamK = 0;
            teamR = 0;
            teamHR = 0;
            teamRBI = 0;
            teamSB = 0;
            for (int i = 0;i < startingPlayers.size();i++) {
                if (startingPlayers.get(i).getIsPitcher()) {
                    teamW += startingPlayers.get(i).getRandW();
                    teamSV += startingPlayers.get(i).getHRandSV();
                    teamK += startingPlayers.get(i).getRBIandK();
                    totalERA += startingPlayers.get(i).getSBandERA();
                    totalWHIP += startingPlayers.get(i).getBAandWHIP();
                }
                else if (!(startingPlayers.get(i).getIsPitcher())) {
                    teamR += startingPlayers.get(i).getRandW();
                    teamHR += startingPlayers.get(i).getHRandSV();
                    teamRBI += startingPlayers.get(i).getRBIandK();
                    teamSB += startingPlayers.get(i).getSBandERA();
                    totalBA += startingPlayers.get(i).getBAandWHIP();
                }
            }
            teamERA = (double)Math.round((totalERA / startingPlayers.size()) * 100) / 100;
            teamWHIP = (double)Math.round((totalWHIP / startingPlayers.size()) * 100) / 100;
            teamBA = (double)Math.round((totalBA / startingPlayers.size()) * 1000) / 1000;
        }
    }
}

