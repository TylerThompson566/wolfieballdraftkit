package wbdk.file;

import wbdk.data.Player;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.ObservableList;

/**
 * This interface provides an abstraction of what a file manager should do. Note
 * that file managers know how to read and write courses, instructors, and subjects,
 * but now how to export sites.
 * 
 * @author Richard McKenna
 */
public interface DraftFileManager {
    public ObservableList<Player>    loadPlayers(String filePathHitters, String filePathPitchers) throws IOException;

    /*
    public void                 saveDraft(Draft courseToSave) throws IOException;
    public void                 loadDraft(Draft courseToLoad, String coursePath) throws IOException;
    public void                 saveTeam(Draft lastInstructor, String filePath) throws IOException;    
    public Team           loadTeam(String filePath) throws IOException;
    
    
    //unsure about these
    public void                 savePlayer(List<Object> subjects, String filePath) throws IOException;
    public ArrayList<String>    loadPlayer(String filePath) throws IOException;
    */
}
