package wbdk.file;

import static wolfieballdraftkit.WBDK_StartupConstants.*;
import wbdk.data.Player;
//import wbdk.data.Team;
//import csb.data.Draft;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonValue;
import wbdk.data.Draft;
import wbdk.data.Team;

/**
 * This is a CourseFileManager that uses the JSON file format to 
 * implement the necessary functions for loading and saving different
 * data for our courses, instructors, and subjects.
 * 
 * @author Tyler
 */
public class JsonDraftFileManager implements DraftFileManager {
    // JSON FILE READING AND WRITING CONSTANTS
    String JSON_TEAM = "TEAM";
    String JSON_FIRST_NAME = "FIRST_NAME";
    String JSON_LAST_NAME = "LAST_NAME";
    String JSON_NOTES = "NOTES";
    String JSON_YOB = "YEAR_OF_BIRTH";
    String JSON_NOB = "NATION_OF_BIRTH";
    String JSON_H = "H";
    
    String JSON_HITTERS = "Hitters";
        String JSON_QP = "QP";
        String JSON_AB = "AB";
        String JSON_R = "R";
        //String JSON_H = "H";
        String JSON_HR = "HR";
        String JSON_RBI = "RBI";
        String JSON_SB = "SB";
    
    String JSON_PITCHERS = "Pitchers";
        String JSON_IP = "IP";
        String JSON_ER = "ER";
        String JSON_W = "W";
        String JSON_SV = "SV";
        //String JSON_H = "H";
        String JSON_BB = "BB";
        String JSON_K = "K";
    
    String JSON_PLAYERS = "Players";
        String JSON_AB_ER = "AB/ER";
        String JSON_R_W = "R/W";
        String JSON_H_H = "H";
        String JSON_HR_SV = "HR/SV";
        String JSON_RBI_K = "RBI/K";
        String JSON_SB_ERA = "SB_ERA";
        String JSON_POSITION = "Position";
    String JSON_EXT = ".json";
    String SLASH = "/";

    
    
    /**
     * This method loads the hitters.json file and the pitchers.json file and creates an arraylist filled with the information
     * from the files.
     * 
     * @param String filePath: The location of the file on disk.
     * 
     * @throws IOException Thrown when there are issues reading the JSON file.
     */
    @Override
    public ObservableList<Player> loadPlayers(String filePathHitters, String filePathPitchers) throws IOException {
        ObservableList<Player> players = FXCollections.observableArrayList();
        JsonObject jsonHitters = loadJSONFile(filePathHitters);
        JsonArray jsonHittersArray = jsonHitters.getJsonArray(JSON_HITTERS);
        for (int i = 0; i < jsonHittersArray.size(); i++) {
            JsonObject jso = jsonHittersArray.getJsonObject(i);
            Player hitter = new Player( jso.getString(JSON_TEAM),
                                        jso.getString(JSON_LAST_NAME),
                                        jso.getString(JSON_FIRST_NAME),
                                        jso.getString(JSON_QP),
                                        0,
                                        Integer.parseInt(jso.getString(JSON_AB)),
                                        Integer.parseInt(jso.getString(JSON_R)),
                                        Integer.parseInt(jso.getString(JSON_H)),
                                        Integer.parseInt(jso.getString(JSON_HR)),
                                        Integer.parseInt(jso.getString(JSON_RBI)),
                                        Integer.parseInt(jso.getString(JSON_SB)),
                                        jso.getString(JSON_NOTES),
                                        Integer.parseInt(jso.getString(JSON_YOB)),
                                        jso.getString(JSON_NOB),
                                        false,
                                        0);
            
            players.add(hitter);        
        }
        JsonObject jsonPitchers = loadJSONFile(filePathPitchers);
        JsonArray jsonPitchersArray = jsonPitchers.getJsonArray(JSON_PITCHERS);
        for (int i = 0; i < jsonPitchersArray.size(); i++) {
            JsonObject jso = jsonPitchersArray.getJsonObject(i);
            Player pitcher = new Player( 
                                        jso.getString(JSON_TEAM),
                                        jso.getString(JSON_LAST_NAME),
                                        jso.getString(JSON_FIRST_NAME),
                                        ("P"),
                                        Double.parseDouble(jso.getString(JSON_IP)),
                                        Integer.parseInt(jso.getString(JSON_ER)),
                                        Integer.parseInt(jso.getString(JSON_W)),
                                        Integer.parseInt(jso.getString(JSON_SV)),
                                        Integer.parseInt(jso.getString(JSON_H)),
                                        Integer.parseInt(jso.getString(JSON_BB)),
                                        Integer.parseInt(jso.getString(JSON_K)),
                                        jso.getString(JSON_NOTES),
                                        Integer.parseInt(jso.getString(JSON_YOB)),
                                        jso.getString(JSON_NOB),
                                        true,
                                        getWHIP(jso.getString(JSON_W), jso.getString(JSON_H), jso.getString(JSON_IP))
                                        );
                                      
            players.add(pitcher);        
        }
        return players;
    }
    //RETURNS BATTING AVERAGE FROM JSON STRINGS
    public double getBA(String jsonH, String jsonAB) {
        int h = Integer.parseInt(jsonH);
        int ab = Integer.parseInt(jsonAB);
        if (ab == 0) {
            return 0;
        }
        else {
            return (h / ab);
        }
    }
    //RETURNS WHIP FROM JSON STRINGS
    public double getWHIP(String jsonW, String jsonH, String jsonIP) {
        int w = Integer.parseInt(jsonW);
        int h = Integer.parseInt(jsonH);
        double ip = Double.parseDouble(jsonIP);
        if (ip == 0) {
            return 0;
        }
        else {
            return ((w + h) / ip);
        }
    }
   
    
    //PRIVATE HELPER METHODS FOR LOADING AND READING JSON FILES
    
    // LOADS A JSON FILE AS A SINGLE OBJECT AND RETURNS IT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }    
    
    // LOADS AN ARRAY OF A SPECIFIC NAME FROM A JSON FILE AND
    // RETURNS IT AS AN ArrayList FULL OF THE DATA FOUND
    private ArrayList<String> loadArrayFromJSONFile(String jsonFilePath, String arrayName) throws IOException {
        JsonObject json = loadJSONFile(jsonFilePath);
        ArrayList<String> items = new ArrayList();
        JsonArray jsonArray = json.getJsonArray(arrayName);
        for (JsonValue jsV : jsonArray) {
            items.add(jsV.toString());
            System.out.println(jsV.toString());
        }
        return items;
    }
    public JsonArray buildJsonArray(List<Object> data) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Object d : data) {
           jsb.add(d.toString());
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    /*
    
    @Override
    public void saveDraft(Draft draftToSave, String draftName) throws IOException {
        // BUILD THE FILE PATH
        String jsonFilePath = PATH_DRAFTS + SLASH + draftName + JSON_EXT;
        
        // INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);  
        
        JsonArray playersJsonArray = makePlayersJsonArray(draftToSave.getPlayers());
        JsonArray teamsJsonArray = makeTeamsJsonArray(draftToSave.getTeams());
        
        // AND AN OBJECT FOR THE INSTRUCTOR
        JsonObject instructorJsonObject = makeInstructorJsonObject(courseToSave.getInstructor());
        
        // ONE FOR EACH OF OUR DATES
        JsonObject startingMondayJsonObject = makeLocalDateJsonObject(courseToSave.getStartingMonday());
        JsonObject endingFridayJsonObject = makeLocalDateJsonObject(courseToSave.getEndingFriday());
        
        // THE LECTURE DAYS ARRAY
        JsonArray lectureDaysJsonArray = makeLectureDaysJsonArray(courseToSave.getLectureDays());
        
        // THE SCHEDULE ITEMS ARRAY
        JsonArray scheduleItemsJsonArray = makeScheduleItemsJsonArray(courseToSave.getScheduleItems());
        
        // THE LECTURES ARRAY
        JsonArray lecturesJsonArray = makeLecturesJsonArray(courseToSave.getLectures());
        
        // THE HWS ARRAY
        JsonArray hwsJsonArray = makeHWsJsonArray(courseToSave.getAssignments());
        
        // NOW BUILD THE COURSE USING EVERYTHING WE'VE ALREADY MADE
        JsonObject courseJsonObject = Json.createObjectBuilder()
                                    .add(JSON_SUBJECT, courseToSave.getSubject().toString())
                                    .add(JSON_NUMBER, courseToSave.getNumber())
                                    .add(JSON_TITLE, courseToSave.getTitle())
                                    .add(JSON_SEMESTER, courseToSave.getSemester().toString())
                                    .add(JSON_YEAR, courseToSave.getYear())
                                    .add(JSON_PAGES, pagesJsonArray)
                                    .add(JSON_INSTRUCTOR, instructorJsonObject)
                                    .add(JSON_STARTING_MONDAY, startingMondayJsonObject)
                                    .add(JSON_ENDING_FRIDAY, endingFridayJsonObject)
                                    .add(JSON_LECTURE_DAYS, lectureDaysJsonArray)
                                    .add(JSON_SCHEDULE_ITEMS, scheduleItemsJsonArray)
                                    .add(JSON_LECTURES, lecturesJsonArray)
                                    .add(JSON_HWS, hwsJsonArray)
                .build();
        
        // AND SAVE EVERYTHING AT ONCE
        jsonWriter.writeObject(courseJsonObject);
    }

    */
    
    public  JsonObject makePlayerJsonObject(Player player) {
        JsonObject jso = Json.createObjectBuilder().add(JSON_TEAM, player.getTeam())
                                                   .add(JSON_LAST_NAME, player.getLastName())
                                                   .add(JSON_FIRST_NAME, player.getFirstName())
                                                   .add(JSON_QP, player.getQP())
                                                   .add(JSON_IP, player.getIP())
                                                   .add(JSON_AB_ER, player.getABandER())
                                                   .add(JSON_R_W, player.getRandW())
                                                   .add(JSON_H, player.getHandH())
                                                   .add(JSON_HR_SV, player.getHRandSV())
                                                   .add(JSON_RBI_K, player.getRBIandK())
                                                   .add(JSON_SB_ERA, player.getSBandERA())
                                                   .add(JSON_POSITION, player.getPosition())
                                                   .add(JSON_NOTES, player.getNotes())
                                                   .add(JSON_YOB, player.getYoB())
                                                   .add(JSON_NOB, player.getNoB())
                                                   .build(); 
        return jso;                
    }
    /*
    public JsonObject makeTeamJsonObject(Team team) {
        JsonObject jso = Json.createObjectBuilder().add(JSON_NAME, team.getName())
                                                    .add(JSON_OWNER, team.getOwner())
                                                    .add(JSON_)
                                                    .build();
        return jso;
    }
    
    public JsonArray makePlayersJsonArray(ObservableList<Player> players) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Player player : players) {
            jsb.add(player.toString());
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    public JsonArray makeTeamsJsonArray(ObservableList<Team> teams) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (Team team : teams) {
            jsb.add(team.toString());
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    */
}
