/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.gui;

import wolfieballdraftkit.WBDK_PropertyType;
import static wbdk.gui.WBDK_GUI.CLASS_HEADING_LABEL;
import static wbdk.gui.WBDK_GUI.CLASS_PROMPT_LABEL;
import static wbdk.gui.WBDK_GUI.PRIMARY_STYLE_SHEET;
import wbdk.data.Player;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wbdk.data.Draft;
import wbdk.data.DraftPick;
import wbdk.data.Team;
import static wbdk.gui.WBDK_GUI.CLASS_SUBHEADING_LABEL;
import static wolfieballdraftkit.WBDK_StartupConstants.*;
/**
 *
 * @author Tyler
 */
public class EditPlayerDialog extends Stage {
    Player player;
    Draft draft;
    Team team;
    String salary;
    ObservableList<String> positions;
    
    Stage primaryStage;
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label nameLabel;
    Label posLabel;
    ImageView playerImageView;
    Image playerImage;
    ImageView nobImageView;
    Image nobImage;
    Label fantasyTeamLabel;
    ComboBox fantasyTeamComboBox;
    Label positionLabel;
    ComboBox positionComboBox;
    Label contractLabel;
    ComboBox contractComboBox;
    Label salaryLabel;
    TextField salaryTextField;
    Button completeButton;
    Button cancelButton;
    Boolean fromPlayersList;
    
    String selection;
    
    public EditPlayerDialog(Stage primaryStage, MessageDialog mesageDialog) {
        player = new Player();
        draft = new Draft();
        team = new Team();
        positions = FXCollections.observableArrayList();
        fromPlayersList = false;
        
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);

        
        
        headingLabel = new Label("PLAYER DETAILS");
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        nameLabel = new Label(player.getFirstName() + " " + player.getLastName());
        nameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        posLabel = new Label(player.getQP());
        posLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        

        playerImageView = new ImageView();

        nobImageView = new ImageView();
        
        fantasyTeamLabel = new Label("Fantasy Team: ");
        fantasyTeamComboBox = new ComboBox();
        fantasyTeamComboBox.getItems().addAll("None");
        for (int i = 0;i < draft.getTeams().size();i++) {
            (fantasyTeamComboBox.getItems()).add(((draft.getTeams()).get(i)).getName());
        }
        fantasyTeamComboBox.getItems().add("None");
        positionLabel = new Label("Position");
        positionComboBox = new ComboBox();
        
        /*
        if ((player.getQP()).contains("C")) {
            positionComboBox.getItems().add("C");
            positions[0] = "C";
        }
        if ((player.getQP()).contains("1B")) {
            positionComboBox.getItems().add("1B");
            positions[1] = "1B";
        }
        if ((player.getQP()).contains("3B")) {
            positionComboBox.getItems().add("3B");
            positions[2] = "3B";
        }
        if ((player.getQP()).contains("2B")) {
            positionComboBox.getItems().add("2B");
            positions[3] = "2B";
        }
        if ((player.getQP()).contains("SS")) {
            positionComboBox.getItems().add("SS");
            positions[4] = "SS";
        }
        if ((player.getQP()).contains("OF")) {
            positionComboBox.getItems().add("OF");
            positions[5] = "OF";
        }
        if ((player.getQP()).contains("P")) {
            positionComboBox.getItems().add("P");
            positions[6] = "P";
        }
        */
                
        contractLabel = new Label("Contract: ");
        contractComboBox = new ComboBox();
        contractComboBox.getItems().addAll("S2", "S1", "X");
        
        salaryLabel = new Label("Salary ($): ");
        salaryTextField = new TextField();
        salaryTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            salary = newValue;
        });
        
        completeButton = new Button("Complete");
        cancelButton = new Button("Cancel");
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            EditPlayerDialog.this.selection = sourceButton.getText();
            EditPlayerDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);

        
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(playerImageView, 0, 1, 1, 4);
        gridPane.add(nobImageView, 1, 1, 1, 1);
        gridPane.add(nameLabel, 1, 2, 1, 1);
        gridPane.add(posLabel, 1, 3, 1, 1);
        gridPane.add(fantasyTeamLabel, 0, 6, 1, 1);
        gridPane.add(fantasyTeamComboBox, 1, 6, 1, 1);
        gridPane.add(positionLabel, 0, 7, 1, 1);
        gridPane.add(positionComboBox, 1, 7, 1, 1);
        gridPane.add(contractLabel, 0, 8, 1, 1);
        gridPane.add(contractComboBox, 1, 8, 1, 1);
        gridPane.add(salaryLabel, 0, 9, 1, 1);
        gridPane.add(salaryTextField, 1, 9, 1, 1);
        gridPane.add(completeButton, 0, 11, 1, 1);
        gridPane.add(cancelButton, 1, 11, 1, 1);
        
        

        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    public String getSelection() {
        return selection;
    }
    public void setSelection(String selection) {
        this.selection = selection;
    }
    public Player getPlayer() {
        return player;
    }
    public Draft getDraft() {
        return draft;
    }
    public String getSalary() {
        return salary;
    }
    
    public Draft showEditPlayerDialog(Player playerToEdit, Draft draftToEdit, Boolean wasFromPlayersList, Boolean wasFromTaxiList, Team teamSelected) {
        setTitle("Edit Player");
        
        Player newPlayer = playerToEdit;
        draft = draftToEdit;
        fromPlayersList = wasFromPlayersList;
    
        String playerImageName = new String("file:" + PATH_PLAYERS + playerToEdit.getLastName() + playerToEdit.getFirstName() + ".jpg");
        playerImage = new Image(playerImageName);
        if (playerImage.isError()) {
            playerImage = new Image("file:" + PATH_PLAYERS + "AAA_PhotoMissing.jpg");
        }
        playerImageView.setImage(playerImage);
        
        String nobImageName = new String("file:" + PATH_PLAYERS + playerToEdit.getNoB() + ".png");
        nobImage = new Image(nobImageName);
        if (nobImage.isError() || nobImageName.equals("") || nobImageName == null) {
            nobImage = new Image("file:" + PATH_PLAYERS + "newFlag.png");
        }
        nobImageView.setImage(nobImage);
        
        ObservableList<String> pos = FXCollections.observableArrayList();
        positions.clear();
        positions.add("None");
        if ((playerToEdit.getQP()).contains("C")) {
            positions.add("C");
        }
        if ((playerToEdit.getQP()).contains("1B")) {
            positions.add("1B");
        }
        if ((playerToEdit.getQP()).contains("3B")) {
            positions.add("3B");
        }
        if (((playerToEdit.getQP()).contains("3B")) && ((playerToEdit.getQP()).contains("1B"))) {
            positions.add("CI");
        }
        if ((playerToEdit.getQP()).contains("2B")) {
            positions.add("2B");
        }
        if ((playerToEdit.getQP()).contains("SS")) {
            positions.add("SS");
        }
        if (((playerToEdit.getQP()).contains("2B")) && ((playerToEdit.getQP()).contains("SS"))) {
            positions.add("MI");
        }
        if ((playerToEdit.getQP()).contains("OF")) {
            positions.add("OF");
        }
        if ((playerToEdit.getQP()).contains("P")) {
            positions.add("P");
        }
        if (!((playerToEdit.getQP()).contains("P"))) {
            positions.add("U");
        }
        
        positionComboBox.setItems(positions);
        
        ObservableList<String> teamNames = FXCollections.observableArrayList();
        for (int i = 0;i < draftToEdit.getTeams().size();i++) {
            teamNames.add((((draftToEdit.getTeams()).get(i)).getName()));
        }
        fantasyTeamComboBox.setItems(teamNames);
        
        
        nameLabel.setText(playerToEdit.getFirstName() + " " + playerToEdit.getLastName());
        posLabel.setText(playerToEdit.getQP());

        
        
        loadGUIData();
        
        this.showAndWait();
        
        
        //set contract and salary
        //place the player in right team and in right position
        boolean validSalary = checkSalary();
        if (!validSalary || (fantasyTeamComboBox.getSelectionModel().getSelectedItem() == null) || (fantasyTeamComboBox.getItems() == null)) {
            MessageDialog messageDialog = new MessageDialog(primaryStage, "Invalid Entries");
            messageDialog.show();
        }
        else {
            if (fromPlayersList) {
                if (   ((String) (positionComboBox.getSelectionModel()).getSelectedItem()).equals("None")  ) {
                    
                }
                else {
                   newPlayer.setPosition((String) (positionComboBox.getSelectionModel()).getSelectedItem());
                    int salaryInt = Integer.parseInt(salary);
                    newPlayer.setSalary(salaryInt);
                    newPlayer.setContract((String) contractComboBox.getSelectionModel().getSelectedItem());
                    
                    System.out.println(newPlayer.getContract() + " " + newPlayer.getPosition() + " " + newPlayer.getFirstName() + " " + newPlayer.getNoB());
                    for (int i = 0;i < draft.getTeams().size();i++) {
                        if ((((draft.getTeams()).get(i)).getName()).equals( (String) fantasyTeamComboBox.getSelectionModel().getSelectedItem())) {
                            boolean added = ((draft.getTeams()).get(i)).addPlayer(newPlayer, (String)positionComboBox.getSelectionModel().getSelectedItem());
                            System.out.println(draft.getTeams().get(i).getMoney() + " " + draft.getTeams().get(i).getTeamR());
                            if (added = true) {
                                draft.getPlayers().remove(newPlayer);
                                DraftPick pick = new DraftPick((draft.getDraftPicks().size() + 1), newPlayer, draft.getTeams().get(i));
                                draft.getDraftPicks().add(pick);
                            }
                            else {
                                
                            }
                            break;
                        }
                    } 
                }
            }
            else if (!fromPlayersList) {
                if (!wasFromTaxiList) {
                    if (   ((String) (positionComboBox.getSelectionModel()).getSelectedItem()).equals("None")  ) {
                        int i = 0;
                        for (i = 0;i < draft.getTeams().size();i++) {
                            if ((((draft.getTeams()).get(i)).getName()).equals( (String) fantasyTeamComboBox.getSelectionModel().getSelectedItem())) {
                                ((draft.getTeams()).get(i)).removeStartingPlayer(newPlayer);
                                newPlayer.setPosition("null");
                                newPlayer.setSalary(0);
                                newPlayer.setContract("X");
                                draft.getPlayers().add(newPlayer);
                                
                                System.out.println(draft.getTeams().get(i).getMoney());
                                break;
                            }
                        }
                    }
                    else {
                        boolean removed = false;
                        for (int i = 0;i < draft.getTeams().size();i++) {
                            for (int k = 0;k < draft.getTeams().get(i).getStartingPlayers().size();k++) {
                                if ((draft.getTeams().get(i).getStartingPlayers().get(k)).equals(newPlayer)) {
                                    draft.getTeams().get(i).removeStartingPlayer(newPlayer);
                                    removed = true;
                                    break;
                                }
                            }
                            if (removed == true) {
                                break;
                            }
                        }
                        if (removed == false) {
                            for (int i = 0;i < draft.getTeams().size();i++) {
                                for (int k = 0;k < draft.getTeams().get(i).getTaxiPlayers().size();i++) {
                                    if ((draft.getTeams().get(i).getTaxiPlayers().get(k)).equals(newPlayer)) {
                                        draft.getTeams().get(i).removeTaxiPlayer(newPlayer);
                                        removed = true;
                                        break;
                                    }
                                }
                                if (removed == true) {
                                    break;
                                }
                            }
                        }
                    
                        if (removed == true) {
                            newPlayer.setPosition((String) (positionComboBox.getSelectionModel()).getSelectedItem());
                            int salaryInt = Integer.parseInt(salary);
                            newPlayer.setSalary(salaryInt);
                            newPlayer.setContract((String) contractComboBox.getSelectionModel().getSelectedItem());
                            System.out.println(newPlayer.getContract() + " " + newPlayer.getPosition() + " " + newPlayer.getFirstName() + " " + newPlayer.getNoB());
                            for (int i = 0;i < draft.getTeams().size();i++) {
                                if ((((draft.getTeams()).get(i)).getName()).equals( (String) fantasyTeamComboBox.getSelectionModel().getSelectedItem())) {
                                ((draft.getTeams()).get(i)).addStartingPlayer(newPlayer, (String)positionComboBox.getSelectionModel().getSelectedItem());
                                System.out.println(draft.getTeams().get(i).getMoney());
                                break;
                            }
                        }
                    }
                }

            }
            else if (wasFromTaxiList) {
                if (   ((String) (positionComboBox.getSelectionModel()).getSelectedItem()).equals("None")  ) {
                        int i = 0;
                        for (i = 0;i < draft.getTeams().size();i++) {
                            if ((((draft.getTeams()).get(i)).getName()).equals( (String) fantasyTeamComboBox.getSelectionModel().getSelectedItem())) {
                                ((draft.getTeams()).get(i)).removeTaxiPlayer(newPlayer);
                                newPlayer.setPosition("null");
                                newPlayer.setSalary(0);
                                newPlayer.setContract("X");
                                draft.getPlayers().add(newPlayer);
                                
                                System.out.println(draft.getTeams().get(i).getMoney());
                                break;
                            }
                        }
                    }
                    else {
                        boolean removed = false;
                        for (int i = 0;i < draft.getTeams().size();i++) {
                            for (int k = 0;k < draft.getTeams().get(i).getTaxiPlayers().size();i++) {
                                if ((draft.getTeams().get(i).getTaxiPlayers().get(k)).equals(newPlayer)) {
                                    draft.getTeams().get(i).removeTaxiPlayer(newPlayer);
                                    removed = true;
                                    break;
                                }
                            }
                            if (removed == true) {
                                break;
                            }
                        }
                        if (removed == false) {
                            for (int i = 0;i < draft.getTeams().size();i++) {
                                for (int k = 0;k < draft.getTeams().get(i).getTaxiPlayers().size();i++) {
                                    if ((draft.getTeams().get(i).getTaxiPlayers().get(k)).equals(newPlayer)) {
                                        draft.getTeams().get(i).removeTaxiPlayer(newPlayer);
                                        removed = true;
                                        break;
                                    }
                                }
                                if (removed == true) {
                                    break;
                                }
                            }
                        }
                    
                        if (removed == true) {
                            newPlayer.setPosition((String) (positionComboBox.getSelectionModel()).getSelectedItem());
                            int salaryInt = Integer.parseInt(salary);
                            newPlayer.setSalary(salaryInt);
                            newPlayer.setContract((String) contractComboBox.getSelectionModel().getSelectedItem());
                            System.out.println(newPlayer.getContract() + " " + newPlayer.getPosition() + " " + newPlayer.getFirstName() + " " + newPlayer.getNoB());
                            for (int i = 0;i < draft.getTeams().size();i++) {
                                if ((((draft.getTeams()).get(i)).getName()).equals( (String) fantasyTeamComboBox.getSelectionModel().getSelectedItem())) {
                                ((draft.getTeams()).get(i)).addStartingPlayer(newPlayer, (String)positionComboBox.getSelectionModel().getSelectedItem());
                                System.out.println(draft.getTeams().get(i).getMoney());
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
        return draft;
    }
    
    public void loadGUIData() {
        // LOAD THE UI STUFF
        if (draft.getTeams().size() == 0) {
            fantasyTeamComboBox.setValue("");
        }
        else {
            fantasyTeamComboBox.setValue(((draft.getTeams()).get(0)).getName());
        }
        if (positions == null || positions.get(0) == null) {
            positions = FXCollections.observableArrayList();
            positions.clear();
            positions.add("None");
            positionComboBox.setValue(positions.get(0));
        }
        else {
            positionComboBox.setValue(positions.get(0));
        }

        contractComboBox.setValue("X");
        salaryTextField.setText("0");
    }
    
    public boolean wasCompleteSelected() {
        if (selection == null) {
            return false;
        }
        
        return selection.equals("Complete");
    }
    
    public boolean checkSalary() {
        for (int i = 0;i < salary.length();i++) {
            if ((salary.charAt(i) < 48) || (salary.charAt(i) > 57)) {
                return false;
            }
            else {
                
            }
        }
        return true;
    }
}
