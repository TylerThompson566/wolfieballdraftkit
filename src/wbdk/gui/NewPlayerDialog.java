/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.gui;

import wolfieballdraftkit.WBDK_PropertyType;
import static wbdk.gui.WBDK_GUI.CLASS_HEADING_LABEL;
import static wbdk.gui.WBDK_GUI.CLASS_PROMPT_LABEL;
import static wbdk.gui.WBDK_GUI.PRIMARY_STYLE_SHEET;
import wbdk.data.Player;
import java.time.LocalDate;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
/**
 *
 * @author Tyler
 */
public class NewPlayerDialog extends Stage {
    //THIS IS THE OBJECT DATA BEHIND THIS UI
    Player player;
    
    //GUI CONTROLS
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label firstNameLabel;
    TextField firstNameTextField;
    Label lastNameLabel;
    TextField lastNameTextField;
    Label proTeamLabel;
    ComboBox proTeamComboBox;
    Label catcherLabel;
    Label firstBaseLabel;
    Label thirdBaseLabel;
    Label secondBaseLabel;
    Label shortstopLabel;
    Label outfielderLabel;
    Label pitcherLabel;
    CheckBox catcherCheckBox;
    CheckBox firstBaseCheckBox;
    CheckBox thirdBaseCheckBox;
    CheckBox secondBaseCheckBox;
    CheckBox shortstopCheckBox;
    CheckBox outfielderCheckBox;
    CheckBox pitcherCheckBox;
    Label completeLabel;
    Label cancelLabel;
    Button completeButton;
    Button cancelButton;
    
    String selection;
    
    public NewPlayerDialog(Stage primaryStage, MessageDialog mesageDialog) {
        player = new Player();
        
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        // PUT THE HEADING IN THE GRID
        headingLabel = new Label("PLAYER DETAILS");
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        //LISTENERS FOR THE TEXT FIELDS
        firstNameLabel = new Label("First Name: ");
        firstNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        firstNameTextField = new TextField();
        firstNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setFirstName(newValue);
        });
        lastNameLabel = new Label("Last Name: ");
        lastNameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        lastNameTextField = new TextField();
        lastNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            player.setLastName(newValue);
        });
       
        //PRO TEAMS LABEL AND COMBO BOX WITH ALL TEAMS IN THE LEAGUE
        proTeamLabel = new Label("Pro Team: ");
        proTeamLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        proTeamComboBox = new ComboBox();
        proTeamComboBox.getItems().addAll("ATL", "AZ", "CHC", "CIN", "COL", "LAD", "MIA", "MIL", "NYM", "PHI", "PIT", "SD", "SF", "STL", "TOR", "WSH");
        
        catcherLabel = new Label("   C");
        catcherCheckBox = new CheckBox();
        firstBaseLabel = new Label("   1B");
        firstBaseCheckBox = new CheckBox();
        thirdBaseLabel = new Label("   3B");
        thirdBaseCheckBox = new CheckBox();
        secondBaseLabel = new Label("   2B");
        secondBaseCheckBox = new CheckBox();
        shortstopLabel = new Label("   SS");
        shortstopCheckBox = new CheckBox();
        outfielderLabel = new Label("   OF");
        outfielderCheckBox = new CheckBox();
        pitcherLabel = new Label("   P");
        pitcherCheckBox = new CheckBox();
        
        // AND FINALLY, THE BUTTONS
        completeButton = new Button("Complete");
        cancelButton = new Button("Cancel");
        
        // REGISTER EVENT HANDLERS FOR OUR BUTTONS
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            NewPlayerDialog.this.selection = sourceButton.getText();
            NewPlayerDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
        //ADD ALL ELEMENTS AT ONCE
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(firstNameLabel, 0, 1, 1, 1);
        gridPane.add(lastNameLabel, 0, 2, 1, 1);
        gridPane.add(proTeamLabel, 0, 3, 1, 1);
        gridPane.add(firstNameTextField, 1, 1, 1, 1);
        gridPane.add(lastNameTextField, 1, 2, 1, 1);
        gridPane.add(proTeamComboBox, 1, 3, 1, 1);
        
        gridPane.add(catcherLabel, 0, 4, 1, 1);
        gridPane.add(catcherCheckBox, 1, 4, 1, 1);
        gridPane.add(firstBaseLabel, 2, 4, 1, 1);
        gridPane.add(firstBaseCheckBox, 3, 4, 1, 1);
        gridPane.add(thirdBaseLabel, 4, 4, 1, 1);
        gridPane.add(thirdBaseCheckBox, 5, 4, 1, 1);
        gridPane.add(secondBaseLabel, 6, 4, 1, 1);
        gridPane.add(secondBaseCheckBox, 7, 4, 1, 1);
        gridPane.add(shortstopLabel, 8, 4, 1, 1);
        gridPane.add(shortstopCheckBox, 9, 4, 1, 1);
        gridPane.add(pitcherLabel, 10, 4, 1, 1);
        gridPane.add(pitcherCheckBox, 11, 4, 1, 1);
        gridPane.add(outfielderLabel, 12, 4, 1, 1);
        gridPane.add(outfielderCheckBox, 13, 4, 1, 1);
        
        gridPane.add(completeButton, 0, 5, 1, 1);
        gridPane.add(cancelButton, 1, 5, 1, 1);
        
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    public String getSelection() {
        return selection;
    }
    
    public Player getPlayer() {
        return player;
    }
    
    public Player showAddPlayerDialog() {
        setTitle("Add New Player");
        
        player = new Player();
        
        loadGUIData();
        
        this.showAndWait();
        
        player.setTeam((proTeamComboBox.getValue()).toString());
        
        StringBuilder positions = new StringBuilder();
        if (catcherCheckBox.isSelected()) {
            positions.append("C_");
        }
        if (firstBaseCheckBox.isSelected()) {
            positions.append("1B_");
        }
        if (thirdBaseCheckBox.isSelected()) {
            positions.append("3B_");
        }
        if (secondBaseCheckBox.isSelected()) {
            positions.append("2B_");
        }
        if (shortstopCheckBox.isSelected()) {
            positions.append("SS_");
        }
        if (outfielderCheckBox.isSelected()) {
            positions.append("OF_");
        }
        if (pitcherCheckBox.isSelected()) {
            positions.append("P_");
            player.setIsPitcher(true);
        }
        
        player.setQP(positions.toString());
        return player;
    }
    
    public void loadGUIData() {
        // LOAD THE UI STUFF
        firstNameTextField.setText(player.getFirstName());
        lastNameTextField.setText(player.getLastName());
        proTeamComboBox.setValue(player.getTeam());
    }
    
    public boolean wasCompleteSelected() {
        return selection.equals("Complete");
    }
}
