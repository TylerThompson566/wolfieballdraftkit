/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.gui;

import wolfieballdraftkit.WBDK_PropertyType;
import wbdk.data.Player;
import wbdk.data.Team;
import static wbdk.gui.WBDK_GUI.CLASS_HEADING_LABEL;
import static wbdk.gui.WBDK_GUI.CLASS_PROMPT_LABEL;
import static wbdk.gui.WBDK_GUI.PRIMARY_STYLE_SHEET;
import java.time.LocalDate;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
/**
 *
 * @author Tyler
 */
public class TeamDialog extends Stage {
    // THIS IS THE OBJECT DATA BEHIND THIS UI
    Team team;
    
    // GUI CONTROLS FOR OUR DIALOG
    GridPane gridPane;
    Scene dialogScene;
    Label headingLabel;
    Label nameLabel;
    TextField nameTextField;
    Label ownerLabel;
    TextField ownerTextField;
    Button completeButton;
    Button cancelButton;
    
    // THIS IS FOR KEEPING TRACK OF WHICH BUTTON THE USER PRESSED
    String selection;
    
    public TeamDialog(Stage primaryStage, MessageDialog messageDialog) {
        team = new Team();
        
        initModality(Modality.WINDOW_MODAL);
        initOwner(primaryStage);
        
        // FIRST OUR CONTAINER
        gridPane = new GridPane();
        gridPane.setPadding(new Insets(10, 20, 20, 20));
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        
        headingLabel = new Label("FANTASY TEAM DETAILS");
        headingLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        
        nameLabel = new Label("Name: ");
        nameLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        nameTextField = new TextField();
        nameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            team.setName(newValue);
        });
        
        ownerLabel = new Label("Owner: ");
        ownerLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        ownerTextField = new TextField();
        ownerTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            team.setOwner(newValue);
        });
        
        completeButton = new Button("Complete");
        cancelButton = new Button("Cancel");
        
        EventHandler completeCancelHandler = (EventHandler<ActionEvent>) (ActionEvent ae) -> {
            Button sourceButton = (Button)ae.getSource();
            TeamDialog.this.selection = sourceButton.getText();
            TeamDialog.this.hide();
        };
        completeButton.setOnAction(completeCancelHandler);
        cancelButton.setOnAction(completeCancelHandler);
        
        gridPane.add(headingLabel, 0, 0, 2, 1);
        gridPane.add(nameLabel, 0, 1, 1, 1);
        gridPane.add(nameTextField, 1, 1, 1, 1);
        gridPane.add(ownerLabel, 0, 2, 1, 1);
        gridPane.add(ownerTextField, 1, 2, 1, 1);
        gridPane.add(completeButton, 0, 3, 1, 1);
        gridPane.add(cancelButton, 1, 3, 1, 1);
        
        dialogScene = new Scene(gridPane);
        dialogScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        this.setScene(dialogScene);
    }
    
    public String getSelection() {
        return selection;
    }
    
    public Team getTeam() {
        return team;
    }
    
    public void loadGUIData() {
        // LOAD THE UI STUFF
        nameTextField.setText(team.getName()); 
        ownerTextField.setText(team.getOwner());
        
    }
    public boolean wasCompleteSelected() {
        return selection.equals("Complete");
    }
    
    public Team showAddTeamDialog() {
        setTitle("Add New Fantasy Team");
        
        team = new Team();
        
        loadGUIData();
        
        this.showAndWait();
        
        team.setName(nameTextField.getText());
        team.setOwner(ownerTextField.getText());
        
        return team;
    }
    
    
    public Team showEditTeamDialog(Team teamToEdit) {
        setTitle("Edit Fantasy Team");
        
        team = new Team();
        team.setName(teamToEdit.getName());
        team.setOwner(teamToEdit.getOwner());
        team.setStartingPlayers(FXCollections.observableArrayList());
        team.setTaxiPlayers(FXCollections.observableArrayList());
        
        loadGUIData();
        
        this.showAndWait();
        team.setName(nameTextField.getText());
        team.setOwner(ownerTextField.getText());
        
        return team;
    }
}
