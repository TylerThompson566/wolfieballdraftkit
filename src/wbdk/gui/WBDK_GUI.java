/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wbdk.gui;

import static wolfieballdraftkit.WBDK_StartupConstants.*;
import wolfieballdraftkit.WBDK_PropertyType;
//import wbdk.controller.CourseEditController;
import wbdk.data.Player; 
import wbdk.data.Team;
import wbdk.data.DraftDataManager;
import wbdk.data.DraftDataView;
//import csb.data.CoursePage;
import wbdk.controller.FileController;
import wbdk.controller.DraftEditController;
import wbdk.file.DraftFileManager;
//import wbdk.file.DraftSiteExporter;
import java.io.IOException;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.ArrayList;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import wbdk.data.Draft;
import wbdk.data.DraftPick;
import wbdk.error.ErrorHandler;
import static wolfieballdraftkit.WBDK_PropertyType.*;
/**
 *
 * @author Tyler
 */
public class WBDK_GUI implements DraftDataView {
    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "wbdk_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_AVAILABLE_PLAYERS = "available_players";
    static final String CLASS_DRAFT_SUMMARY = "draft_summary";
    static final String CLASS_FANTASY_STANDINGS = "fantasy_standings";
    static final String CLASS_FANTASY_TEAMS = "fantasy_teams";
    static final String CLASS_MLB_TEAMS = "mlb_teams";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;
    
    // THIS MANAGES ALL OF THE APPLICATION'S DATA
    DraftDataManager dataManager;

    // THIS MANAGES COURSE FILE I/O
    DraftFileManager draftFileManager;

    // THIS MANAGES EXPORTING OUR SITE PAGES
    //DraftSiteExporter siteExporter;

    // THIS HANDLES INTERACTIONS WITH FILE-RELATED CONTROLS
    FileController fileController;

    // THIS HANDLES INTERACTIONS WITH COURSE INFO CONTROLS
    DraftEditController draftController;
    
    // THIS HANDLES REQUESTS TO ADD OR EDIT SCHEDULE STUFF
    //TeamEditController scheduleController;

    
    // THIS IS THE APPLICATION WINDOW
    Stage primaryStage;

    // THIS IS THE STAGE'S SCENE GRAPH
    Scene initialScene;
    Scene fantasyTeamsScene;
    Scene availablePlayersScene;
    Scene fantasyStandingsScene;
    Scene draftSummaryScene;
    Scene MLBTeamsScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane initialBorderPane;
    BorderPane fantasyTeamsBorderPane;
    BorderPane availablePlayersBorderPane;
    BorderPane fantasyStandingsBorderPane;
    BorderPane draftSummaryBorderPane;
    BorderPane MLBTeamsBorderPane;
    boolean initialWorkspaceActivated;
    boolean fantasyTeamsWorkspaceActivated;
    boolean availablePlayersWorkspaceActivated;
    boolean fantasyStandingsWorkspaceActivated;
    boolean draftSummaryWorkspaceActivated;
    boolean MLBTeamsWorkspaceActivated;
    
    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane initialFileToolbarPane;
    Button initialNewDraftButton;
    Button initialLoadDraftButton;
    Button initialSaveDraftButton;
    Button initialExportSiteButton;
    Button initialExitButton;
    
    FlowPane fantasyTeamsFileToolbarPane;
    Button fantasyTeamsNewDraftButton;
    Button fantasyTeamsLoadDraftButton;
    Button fantasyTeamsSaveDraftButton;
    Button fantasyTeamsExportSiteButton;
    Button fantasyTeamsExitButton;
    
    FlowPane availablePlayersFileToolbarPane;
    Button availablePlayersNewDraftButton;
    Button availablePlayersLoadDraftButton;
    Button availablePlayersSaveDraftButton;
    Button availablePlayersExportSiteButton;
    Button availablePlayersExitButton;
    
    FlowPane fantasyStandingsFileToolbarPane;
    Button fantasyStandingsNewDraftButton;
    Button fantasyStandingsLoadDraftButton;
    Button fantasyStandingsSaveDraftButton;
    Button fantasyStandingsExportSiteButton;
    Button fantasyStandingsExitButton;
    
    FlowPane draftSummaryFileToolbarPane;
    Button draftSummaryNewDraftButton;
    Button draftSummaryLoadDraftButton;
    Button draftSummarySaveDraftButton;
    Button draftSummaryExportSiteButton;
    Button draftSummaryExitButton;
    
    FlowPane MLBTeamsFileToolbarPane;
    Button MLBTeamsNewDraftButton;
    Button MLBTeamsLoadDraftButton;
    Button MLBTeamsSaveDraftButton;
    Button MLBTeamsExportSiteButton;
    Button MLBTeamsExitButton;
    
    //THIS IS THE BOTTOM TOOLBAR ADN ITS CONTROLS
    FlowPane fantasyTeamsFantasyToolbarPane;
    Button fantasyTeamsFantasyTeamsButton;
    Button fantasyTeamsAvailablePlayersButton;
    Button fantasyTeamsFantasyStandingsButton;
    Button fantasyTeamsDraftSummaryButton;
    Button fantasyTeamsMLBTeamsButton;
    
    FlowPane availablePlayersFantasyToolbarPane;
    Button availablePlayersFantasyTeamsButton;
    Button availablePlayersAvailablePlayersButton;
    Button availablePlayersFantasyStandingsButton;
    Button availablePlayersDraftSummaryButton;
    Button availablePlayersMLBTeamsButton;
    
    FlowPane fantasyStandingsFantasyToolbarPane;
    Button fantasyStandingsFantasyTeamsButton;
    Button fantasyStandingsAvailablePlayersButton;
    Button fantasyStandingsFantasyStandingsButton;
    Button fantasyStandingsDraftSummaryButton;
    Button fantasyStandingsMLBTeamsButton;
    
    FlowPane MLBTeamsFantasyToolbarPane;
    Button MLBTeamsFantasyTeamsButton;
    Button MLBTeamsAvailablePlayersButton;
    Button MLBTeamsFantasyStandingsButton;
    Button MLBTeamsDraftSummaryButton;
    Button MLBTeamsMLBTeamsButton;
    
    FlowPane draftSummaryFantasyToolbarPane;
    Button draftSummaryFantasyTeamsButton;
    Button draftSummaryAvailablePlayersButton;
    Button draftSummaryFantasyStandingsButton;
    Button draftSummaryDraftSummaryButton;
    Button draftSummaryMLBTeamsButton;
    
    
    
    //THIS IS EVERYTHING GOING IN THE DRAFT SUMMARY STANDINGS SCENE
        Label draftSummaryLabel;
        ScrollPane draftSummaryScrollPane;
        BorderPane draftSummaryWorkspaceBorderPane;
        GridPane draftSummaryTopGridPane;
        Button draftSummaryStarButton;
        Button draftSummaryStartDraftButton;
        Button draftSummaryStopDraftButton;
        TableView draftSummaryTable;
        TableColumn draftSummaryPickNumberColumn;
        TableColumn draftSummaryFirstColumn;
        TableColumn draftSummaryLastColumn;
        TableColumn draftSummaryTeamColumn;
        TableColumn draftSummaryContractColumn;
        TableColumn draftSummarySalaryColumn;
        boolean runningAutoDraft = false;
    
    //THIS IS EVERYHTING GOING IN THE FANTASY STANDINGS SCENE
    Label fantasyStandingsLabel;
    ScrollPane fantasyStandingsScrollPane;
    BorderPane fantasyStandingsWorkspaceBorderPane;
    TableView fantasyStandingsTable;
    TableColumn fantasyStandingsNameColumn;
    TableColumn fantasyStandingsPlayersNeededColumn;
    TableColumn fantasyStandingsMoneyColumn;
    TableColumn fantasyStandingsMoneyPerPlayerColumn;
    TableColumn fantasyStandingsRColumn;
    TableColumn fantasyStandingsHRColumn;
    TableColumn fantasyStandingsRBIColumn;
    TableColumn fantasyStandingsSBColumn;
    TableColumn fantasyStandingsBAColumn;
    TableColumn fantasyStandingsWColumn;
    TableColumn fantasyStandingsSVColumn;
    TableColumn fantasyStandingsKColumn;
    TableColumn fantasyStandingsERAColumn;
    TableColumn fantasyStandingsWHIPColumn;
    TableColumn fantasyStandingsTotalPointsColumn;
    
    //THGIS IS EVERYTHING GOING IN THE MLB TEAMS SCENE
    
        Label MLBTeamsLabel;
        ScrollPane MLBTeamsScrollPane;
        BorderPane MLBTeamsWorkspaceBorderPane;
        GridPane MLBTeamsControlGridPane;
        ComboBox MLBTeamsComboBox;
        TableView MLBTeamsTable;
        TableColumn MLBTeamsPositionColumn;
        TableColumn MLBTeamsFirstColumn;
        TableColumn MLBTeamsLastColumn;
        TableColumn MLBTeamsProTeamColumn;
        TableColumn MLBTeamsPositionsColumn;
        TableColumn MLBTeamsYobColumn;
        TableColumn MLBTeamsRWColumn;
        TableColumn MLBTeamsHrSvColumn;
        TableColumn MLBTeamsRbiKColumn;
        TableColumn MLBTeamsSbEraColumn;
        TableColumn MLBTeamsBaWhipColumn;
        TableColumn MLBTeamsEstValueColumn;
        TableColumn MLBTeamsNotesColumn;
        Label MLBTeamsSelectLabel;
    
    //THIS IS EVERYTHING GOING IN THE FANTASY TEAMS SCENE
    BorderPane fantasyTeamsWorkspaceBorderPane;
    ScrollPane fantasyTeamsScrollPane;
        //TOP PART
        GridPane fantasyTeamsControlsGridPane;
        Label fantasyTeamsLabel;
        Label draftNameLabel;
        TextField draftNameTextField;
        Button addTeamButton;
        Button removeTeamButton;
        Button editTeamButton;
        Label selectFantasyTeamLabel;
        ComboBox fantasyTeamComboBox;
        
        //STARTING LINEUP PART
        VBox startingLineupVBox;
        Label startingLineupLabel;
        TableView<Player> startingLineupTeamTable;
        TableColumn startingLineupPositionColumn;
        TableColumn startingLineupFirstColumn;
        TableColumn startingLineupLastColumn;
        TableColumn startingLineupProTeamColumn;
        TableColumn startingLineupPositionsColumn;
        TableColumn startingLineupYobColumn;
        TableColumn startingLineupRWColumn;
        TableColumn startingLineupHrSvColumn;
        TableColumn startingLineupRbiKColumn;
        TableColumn startingLineupSbEraColumn;
        TableColumn startingLineupBaWhipColumn;
        TableColumn startingLineupEstValueColumn;
        TableColumn startingLineupNotesColumn;
        
        //TAXI DRAFT PART
        VBox taxiDraftVBox;
        Label taxiDraftLabel;
        TableView<Player> taxiDraftTeamTable;
        TableColumn taxiDraftPositionColumn;
        TableColumn taxiDraftFirstColumn;
        TableColumn taxiDraftLastColumn;
        TableColumn taxiDraftProTeamColumn;
        TableColumn taxiDraftPositionsColumn;
        TableColumn taxiDraftYobColumn;
        TableColumn taxiDraftRWColumn;
        TableColumn taxiDraftHrSvColumn;
        TableColumn taxiDraftRbiKColumn;
        TableColumn taxiDraftSbEraColumn;
        TableColumn taxiDraftBaWhipColumn;
        TableColumn taxiDraftEstValueColumn;
        TableColumn taxiDraftNotesColumn;
        
        
    
    
    //THIS IS EVERYTHING GOING IN THE AVAILABLE PLAYERS SCENE
    ScrollPane availablePlayersScrollPane;
        //TOP PART
        FlowPane playerListControlsFlowPane;
        BorderPane availablePlayersWorkspaceBorderPane;
        Label availablePlayersLabel;
        Button addPlayerButton;
        Button removePlayerButton;
        Label searchLabel;
        TextField searchTextField;
    
        //RADIO BUTTONS AND LABELS
        HBox searchButtonsHBox;
        Label sortLabel;
        Label allButtonLabel;
        Button allButton;
        Label catcherButtonLabel;
        Button catcherButton;
        Label firstBaseButtonLabel;
        Button firstBaseButton;
        Label cornerInfielderButtonLabel;
        Button cornerInfielderButton;
        Label thirdBaseButtonLabel;
        Button thirdBaseButton;
        Label secondBaseButtonLabel;
        Button secondBaseButton;
        Label middleInfielderbuttonLabel;
        Button middleInfielderButton;
        Label shortStopButtonLabel;
        Button shortStopButton;
        Label outfielderButtonLabel;
        Button outfielderButton;
        Label utilitybuttonLabel;
        Button utilityButton;
        Label pitcherButtonLabel;
        Button pitcherButton;
    
        //TABLE AND COLUMN STRINGS
        FlowPane tableFlowPane;
        TableView<Player> playersList;
        TableColumn firstColumn;
        TableColumn lastColumn;
        TableColumn proTeamColumn;
        TableColumn positionsColumn;
        TableColumn yobColumn;
        TableColumn rWColumn;
        TableColumn hrSvColumn;
        TableColumn rbiKColumn;
        TableColumn sbEraColumn;
        TableColumn baWhipColumn;
        TableColumn estValueColumn;
        TableColumn notesColumn;
        static final String COL_FIRST = "First";
        static final String COL_LAST = "Last";
        static final String COL_PRO_TEAM = "Pro Team";
        static final String COL_POSITION = "Position";
        static final String COL_POSITIONS = "Positions";
        static final String COL_YOB = "Year of Birth";
        static final String COL_R_W = "R/W";
        static final String COL_HR_SV = "HR/SV";
        static final String COL_RBI_K = "RBI/K";
        static final String COL_SB_ERA = "SB/ERA";
        static final String COL_BA_WHIP = "BA/WHIP";
        static final String COL_EST_VALUE = "Estimated Value";
        static final String COL_NOTES = "Notes";
        
    //DIALOGS
    ErrorHandler errorHandler;
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    
    // WE'LL USE THIS TO GET OUR VERIFICATION FEEDBACK
    PropertiesManager properties;
    
    public WBDK_GUI(Stage initPrimaryStage) {
        primaryStage = initPrimaryStage;
    }
    
    public DraftDataManager getDataManager() {
        return dataManager;
    }
    
    public FileController getFileController() {
        return fileController;
    }
    
    public PropertiesManager getPropertiesManager() {
        return properties;
    }
    
    public DraftFileManager getDraftFileManager() {
        return draftFileManager;
    }
    
    //public DraftSiteExporter getSiteExporter() {
    //    return siteExporter;
    //}
    
    public Stage getWindow() {
        return primaryStage;
    }
    
    public MessageDialog getMessageDialog() {
        return messageDialog;
    }
    
    public YesNoCancelDialog getYesNoCancelDialog() {
        return yesNoCancelDialog;
    }
    
    public void setDataManager(DraftDataManager initDataManager) {
        dataManager = initDataManager;
    }
    
    public void setDraftFileManager(DraftFileManager initDraftFileManager) {
        draftFileManager = initDraftFileManager;
    }

    //public void setSiteExporter(DraftSiteExporter initSiteExporter) {
    //    siteExporter = initSiteExporter;
    //}
    
    public void initGUI(String windowTitle) throws IOException {
        // INIT THE DIALOGS
        initDialogs();
        
        
        // INIT THE TOOLBARS
        initFileToolbar();
        initFantasyToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        initInitialWorkspace();
        initAvailablePlayersWorkspace();
        initFantasyTeamsWorkspace();
        initFantasyStandingsWorkspace();
        initDraftSummaryWorkspace();
        initMLBTeamsWorkspace();
        // NOW SETUP THE EVENT HANDLERS
        initEventHandlers();

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        initWindow(windowTitle);
    }
    
    
    
    /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR GUI */
    /****************************************************************************/
    
    private void initDialogs() {
        properties = PropertiesManager.getPropertiesManager();
        messageDialog = new MessageDialog(primaryStage, CLOSE_BUTTON_LABEL);
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
        fileController = new FileController(messageDialog, yesNoCancelDialog, draftFileManager);
    }
    
    private void initFileToolbar() {
        
        initialFileToolbarPane = new FlowPane();
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        initialNewDraftButton = initChildButton(WBDK_PropertyType.NEW_DRAFT_ICON, WBDK_PropertyType.NEW_DRAFT_TOOLTIP, false);
        initialLoadDraftButton = initChildButton(WBDK_PropertyType.LOAD_DRAFT_ICON, WBDK_PropertyType.LOAD_DRAFT_TOOLTIP, false);
        initialSaveDraftButton = initChildButton(WBDK_PropertyType.SAVE_DRAFT_ICON, WBDK_PropertyType.SAVE_DRAFT_TOOLTIP, false);
        initialExportSiteButton = initChildButton(WBDK_PropertyType.EXPORT_PAGE_ICON, WBDK_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        initialExitButton = initChildButton(WBDK_PropertyType.EXIT_ICON, WBDK_PropertyType.EXIT_TOOLTIP, false);
        initialFileToolbarPane.getChildren().addAll(initialNewDraftButton, initialLoadDraftButton, initialSaveDraftButton, initialExportSiteButton, initialExitButton);
        
        fantasyStandingsFileToolbarPane = new FlowPane();
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        fantasyStandingsNewDraftButton = initChildButton(WBDK_PropertyType.NEW_DRAFT_ICON, WBDK_PropertyType.NEW_DRAFT_TOOLTIP, false);
        fantasyStandingsLoadDraftButton = initChildButton(WBDK_PropertyType.LOAD_DRAFT_ICON, WBDK_PropertyType.LOAD_DRAFT_TOOLTIP, false);
        fantasyStandingsSaveDraftButton = initChildButton(WBDK_PropertyType.SAVE_DRAFT_ICON, WBDK_PropertyType.SAVE_DRAFT_TOOLTIP, false);
        fantasyStandingsExportSiteButton = initChildButton(WBDK_PropertyType.EXPORT_PAGE_ICON, WBDK_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        fantasyStandingsExitButton = initChildButton(WBDK_PropertyType.EXIT_ICON, WBDK_PropertyType.EXIT_TOOLTIP, false);
        fantasyStandingsFileToolbarPane.getChildren().addAll(fantasyStandingsNewDraftButton, fantasyStandingsLoadDraftButton, fantasyStandingsSaveDraftButton, fantasyStandingsExportSiteButton, fantasyStandingsExitButton);
        
        
        
        fantasyTeamsFileToolbarPane = new FlowPane();
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        fantasyTeamsNewDraftButton = initChildButton(WBDK_PropertyType.NEW_DRAFT_ICON, WBDK_PropertyType.NEW_DRAFT_TOOLTIP, false);
        fantasyTeamsLoadDraftButton = initChildButton(WBDK_PropertyType.LOAD_DRAFT_ICON, WBDK_PropertyType.LOAD_DRAFT_TOOLTIP, false);
        fantasyTeamsSaveDraftButton = initChildButton(WBDK_PropertyType.SAVE_DRAFT_ICON, WBDK_PropertyType.SAVE_DRAFT_TOOLTIP, false);
        fantasyTeamsExportSiteButton = initChildButton(WBDK_PropertyType.EXPORT_PAGE_ICON, WBDK_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        fantasyTeamsExitButton = initChildButton(WBDK_PropertyType.EXIT_ICON, WBDK_PropertyType.EXIT_TOOLTIP, false);
        fantasyTeamsFileToolbarPane.getChildren().addAll(fantasyTeamsNewDraftButton, fantasyTeamsLoadDraftButton, fantasyTeamsSaveDraftButton, fantasyTeamsExportSiteButton, fantasyTeamsExitButton);
        
        
        availablePlayersFileToolbarPane = new FlowPane();
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        availablePlayersNewDraftButton = initChildButton(WBDK_PropertyType.NEW_DRAFT_ICON, WBDK_PropertyType.NEW_DRAFT_TOOLTIP, false);
        availablePlayersLoadDraftButton = initChildButton(WBDK_PropertyType.LOAD_DRAFT_ICON, WBDK_PropertyType.LOAD_DRAFT_TOOLTIP, false);
        availablePlayersSaveDraftButton = initChildButton(WBDK_PropertyType.SAVE_DRAFT_ICON, WBDK_PropertyType.SAVE_DRAFT_TOOLTIP, false);
        availablePlayersExportSiteButton = initChildButton(WBDK_PropertyType.EXPORT_PAGE_ICON, WBDK_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        availablePlayersExitButton = initChildButton(WBDK_PropertyType.EXIT_ICON, WBDK_PropertyType.EXIT_TOOLTIP, false);
        availablePlayersFileToolbarPane.getChildren().addAll(availablePlayersNewDraftButton, availablePlayersLoadDraftButton, availablePlayersSaveDraftButton, availablePlayersExportSiteButton, availablePlayersExitButton);
        
        
        draftSummaryFileToolbarPane = new FlowPane();
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        draftSummaryNewDraftButton = initChildButton(WBDK_PropertyType.NEW_DRAFT_ICON, WBDK_PropertyType.NEW_DRAFT_TOOLTIP, false);
        draftSummaryLoadDraftButton = initChildButton(WBDK_PropertyType.LOAD_DRAFT_ICON, WBDK_PropertyType.LOAD_DRAFT_TOOLTIP, false);
        draftSummarySaveDraftButton = initChildButton(WBDK_PropertyType.SAVE_DRAFT_ICON, WBDK_PropertyType.SAVE_DRAFT_TOOLTIP, false);
        draftSummaryExportSiteButton = initChildButton(WBDK_PropertyType.EXPORT_PAGE_ICON, WBDK_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        draftSummaryExitButton = initChildButton(WBDK_PropertyType.EXIT_ICON, WBDK_PropertyType.EXIT_TOOLTIP, false);
        draftSummaryFileToolbarPane.getChildren().addAll(draftSummaryNewDraftButton, draftSummaryLoadDraftButton, draftSummarySaveDraftButton, draftSummaryExportSiteButton, draftSummaryExitButton);
        
        
        MLBTeamsFileToolbarPane = new FlowPane();
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        MLBTeamsNewDraftButton = initChildButton(WBDK_PropertyType.NEW_DRAFT_ICON, WBDK_PropertyType.NEW_DRAFT_TOOLTIP, false);
        MLBTeamsLoadDraftButton = initChildButton(WBDK_PropertyType.LOAD_DRAFT_ICON, WBDK_PropertyType.LOAD_DRAFT_TOOLTIP, false);
        MLBTeamsSaveDraftButton = initChildButton(WBDK_PropertyType.SAVE_DRAFT_ICON, WBDK_PropertyType.SAVE_DRAFT_TOOLTIP, false);
        MLBTeamsExportSiteButton = initChildButton(WBDK_PropertyType.EXPORT_PAGE_ICON, WBDK_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        MLBTeamsExitButton = initChildButton(WBDK_PropertyType.EXIT_ICON, WBDK_PropertyType.EXIT_TOOLTIP, false);
        MLBTeamsFileToolbarPane.getChildren().addAll(MLBTeamsNewDraftButton, MLBTeamsLoadDraftButton, MLBTeamsSaveDraftButton, MLBTeamsExportSiteButton, MLBTeamsExitButton);
        
        
        
    }
    
    private void initFantasyToolbar() {
        fantasyTeamsFantasyToolbarPane = new FlowPane();
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        fantasyTeamsFantasyTeamsButton = initChildButton(WBDK_PropertyType.FANTASY_TEAMS_ICON, WBDK_PropertyType.FANTASY_TEAMS_TOOLTIP, false);
        fantasyTeamsAvailablePlayersButton = initChildButton(WBDK_PropertyType.AVAILABLE_PLAYERS_ICON, WBDK_PropertyType.AVAILABLE_PLAYERS_TOOLTIP, false);
        fantasyTeamsFantasyStandingsButton = initChildButton(WBDK_PropertyType.FANTASY_STANDINGS_ICON, WBDK_PropertyType.FANTASY_STANDINGS_TOOLTIP, false);
        fantasyTeamsDraftSummaryButton = initChildButton(WBDK_PropertyType.DRAFT_SUMMARY_ICON, WBDK_PropertyType.DRAFT_SUMMARY_TOOLTIP, false);
        fantasyTeamsMLBTeamsButton = initChildButton(WBDK_PropertyType.MLB_TEAMS_ICON, WBDK_PropertyType.MLB_TEAMS_TOOLTIP, false);
        fantasyTeamsFantasyToolbarPane.getChildren().addAll(fantasyTeamsFantasyTeamsButton, fantasyTeamsAvailablePlayersButton, fantasyTeamsFantasyStandingsButton, fantasyTeamsDraftSummaryButton, fantasyTeamsMLBTeamsButton);
        
        
        availablePlayersFantasyToolbarPane = new FlowPane();
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        availablePlayersFantasyTeamsButton = initChildButton(WBDK_PropertyType.FANTASY_TEAMS_ICON, WBDK_PropertyType.FANTASY_TEAMS_TOOLTIP, false);
        availablePlayersAvailablePlayersButton = initChildButton(WBDK_PropertyType.AVAILABLE_PLAYERS_ICON, WBDK_PropertyType.AVAILABLE_PLAYERS_TOOLTIP, false);
        availablePlayersFantasyStandingsButton = initChildButton(WBDK_PropertyType.FANTASY_STANDINGS_ICON, WBDK_PropertyType.FANTASY_STANDINGS_TOOLTIP, false);
        availablePlayersDraftSummaryButton = initChildButton(WBDK_PropertyType.DRAFT_SUMMARY_ICON, WBDK_PropertyType.DRAFT_SUMMARY_TOOLTIP, false);
        availablePlayersMLBTeamsButton = initChildButton(WBDK_PropertyType.MLB_TEAMS_ICON, WBDK_PropertyType.MLB_TEAMS_TOOLTIP, false);
        availablePlayersFantasyToolbarPane.getChildren().addAll(availablePlayersFantasyTeamsButton, availablePlayersAvailablePlayersButton, availablePlayersFantasyStandingsButton, availablePlayersDraftSummaryButton, availablePlayersMLBTeamsButton);
    
    
        fantasyStandingsFantasyToolbarPane = new FlowPane();
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        fantasyStandingsFantasyTeamsButton = initChildButton(WBDK_PropertyType.FANTASY_TEAMS_ICON, WBDK_PropertyType.FANTASY_TEAMS_TOOLTIP, false);
        fantasyStandingsAvailablePlayersButton = initChildButton(WBDK_PropertyType.AVAILABLE_PLAYERS_ICON, WBDK_PropertyType.AVAILABLE_PLAYERS_TOOLTIP, false);
        fantasyStandingsFantasyStandingsButton = initChildButton(WBDK_PropertyType.FANTASY_STANDINGS_ICON, WBDK_PropertyType.FANTASY_STANDINGS_TOOLTIP, false);
        fantasyStandingsDraftSummaryButton = initChildButton(WBDK_PropertyType.DRAFT_SUMMARY_ICON, WBDK_PropertyType.DRAFT_SUMMARY_TOOLTIP, false);
        fantasyStandingsMLBTeamsButton = initChildButton(WBDK_PropertyType.MLB_TEAMS_ICON, WBDK_PropertyType.MLB_TEAMS_TOOLTIP, false);
        fantasyStandingsFantasyToolbarPane.getChildren().addAll(fantasyStandingsFantasyTeamsButton, fantasyStandingsAvailablePlayersButton, fantasyStandingsFantasyStandingsButton, fantasyStandingsDraftSummaryButton, fantasyStandingsMLBTeamsButton);
        
        
        draftSummaryFantasyToolbarPane = new FlowPane();
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        draftSummaryFantasyTeamsButton = initChildButton(WBDK_PropertyType.FANTASY_TEAMS_ICON, WBDK_PropertyType.FANTASY_TEAMS_TOOLTIP, false);
        draftSummaryAvailablePlayersButton = initChildButton(WBDK_PropertyType.AVAILABLE_PLAYERS_ICON, WBDK_PropertyType.AVAILABLE_PLAYERS_TOOLTIP, false);
        draftSummaryFantasyStandingsButton = initChildButton(WBDK_PropertyType.FANTASY_STANDINGS_ICON, WBDK_PropertyType.FANTASY_STANDINGS_TOOLTIP, false);
        draftSummaryDraftSummaryButton = initChildButton(WBDK_PropertyType.DRAFT_SUMMARY_ICON, WBDK_PropertyType.DRAFT_SUMMARY_TOOLTIP, false);
        draftSummaryMLBTeamsButton = initChildButton(WBDK_PropertyType.MLB_TEAMS_ICON, WBDK_PropertyType.MLB_TEAMS_TOOLTIP, false);
        draftSummaryFantasyToolbarPane.getChildren().addAll(draftSummaryFantasyTeamsButton, draftSummaryAvailablePlayersButton, draftSummaryFantasyStandingsButton, draftSummaryDraftSummaryButton, draftSummaryMLBTeamsButton);
        
        
        MLBTeamsFantasyToolbarPane = new FlowPane();
        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        MLBTeamsFantasyTeamsButton = initChildButton(WBDK_PropertyType.FANTASY_TEAMS_ICON, WBDK_PropertyType.FANTASY_TEAMS_TOOLTIP, false);
        MLBTeamsAvailablePlayersButton = initChildButton(WBDK_PropertyType.AVAILABLE_PLAYERS_ICON, WBDK_PropertyType.AVAILABLE_PLAYERS_TOOLTIP, false);
        MLBTeamsFantasyStandingsButton = initChildButton(WBDK_PropertyType.FANTASY_STANDINGS_ICON, WBDK_PropertyType.FANTASY_STANDINGS_TOOLTIP, false);
        MLBTeamsDraftSummaryButton = initChildButton(WBDK_PropertyType.DRAFT_SUMMARY_ICON, WBDK_PropertyType.DRAFT_SUMMARY_TOOLTIP, false);
        MLBTeamsMLBTeamsButton = initChildButton(WBDK_PropertyType.MLB_TEAMS_ICON, WBDK_PropertyType.MLB_TEAMS_TOOLTIP, false);
        MLBTeamsFantasyToolbarPane.getChildren().addAll(MLBTeamsFantasyTeamsButton, MLBTeamsAvailablePlayersButton, MLBTeamsFantasyStandingsButton, MLBTeamsDraftSummaryButton, MLBTeamsMLBTeamsButton);
    }
    
    
    //ALL METHODS BELOW UNTIL NEXT SET OF METHODS ARE FOR FANTASY TEAMS WORKSPACE
    private void initFantasyTeamsWorkspace() throws IOException {
        
        initTeamControlsWorkspace();
        
        initStartingLineupTableWorkspace();
        
        initTaxiSquadTableWorkspace();
        // THE WORKSPACE HAS A FEW REGIONS, THIS 
        // IS FOR BASIC COURSE EDITING CONTROLS
        
        
        fantasyTeamsWorkspaceBorderPane = new BorderPane();
        fantasyTeamsWorkspaceBorderPane.setTop(fantasyTeamsControlsGridPane);
        fantasyTeamsWorkspaceBorderPane.setCenter(startingLineupVBox);
        fantasyTeamsWorkspaceBorderPane.setBottom(taxiDraftVBox);
        fantasyTeamsScrollPane = new ScrollPane();
        fantasyTeamsScrollPane.setContent(fantasyTeamsWorkspaceBorderPane);
        fantasyTeamsScrollPane.setFitToWidth(true);
        fantasyTeamsBorderPane = new BorderPane();
        fantasyTeamsBorderPane.setTop(fantasyTeamsFileToolbarPane);
        fantasyTeamsBorderPane.setCenter(fantasyTeamsScrollPane);
        fantasyTeamsBorderPane.setBottom(fantasyTeamsFantasyToolbarPane);
        fantasyTeamsBorderPane.getStyleClass().add(CLASS_FANTASY_TEAMS);
        fantasyTeamsScrollPane.getStyleClass().add(CLASS_FANTASY_TEAMS);
        fantasyTeamsWorkspaceBorderPane.getStyleClass().add(CLASS_FANTASY_TEAMS);
        
    }
    
    public void initTeamControlsWorkspace() {
        fantasyTeamsControlsGridPane = new GridPane();
        fantasyTeamsControlsGridPane.getStyleClass().add(CLASS_FANTASY_TEAMS);
        fantasyTeamsLabel = initLabel(WBDK_PropertyType.FANTASY_TEAMS_HEADING_LABEL, CLASS_HEADING_LABEL);
        draftNameLabel = new Label("Draft Name: ");
        draftNameLabel.getStylesheets().add(CLASS_PROMPT_LABEL);
        draftNameTextField = new TextField();
        addTeamButton = initChildButton(WBDK_PropertyType.ADD_ICON, WBDK_PropertyType.ADD_TEAM_TOOLTIP, false);
        removeTeamButton = initChildButton(WBDK_PropertyType.MINUS_ICON, WBDK_PropertyType.REMOVE_TEAM_TOOLTIP, false);
        editTeamButton = initChildButton(WBDK_PropertyType.EDIT_ICON, WBDK_PropertyType.EDIT_TEAM_TOOLTIP, false);
        selectFantasyTeamLabel = new Label("Select Fantasy Team: ");
        selectFantasyTeamLabel.getStylesheets().add(CLASS_PROMPT_LABEL);
        fantasyTeamComboBox = new ComboBox();
        
        fantasyTeamsControlsGridPane.add(fantasyTeamsLabel, 0, 0, 4, 1);
        fantasyTeamsControlsGridPane.add(draftNameLabel, 1, 1, 1, 1);
        fantasyTeamsControlsGridPane.add(draftNameTextField, 2, 1, 1, 1);
        fantasyTeamsControlsGridPane.add(addTeamButton, 0, 2, 1, 1);
        fantasyTeamsControlsGridPane.add(removeTeamButton, 1, 2, 1, 1);
        fantasyTeamsControlsGridPane.add(editTeamButton, 2, 2, 1, 1);
        fantasyTeamsControlsGridPane.add(selectFantasyTeamLabel, 3, 2, 1, 1);
        fantasyTeamsControlsGridPane.add(fantasyTeamComboBox, 4, 2, 1, 1);
        
        loadTeamsComboBox();
    }
    //LOADS ALL OF THE TEAMS CREATED TO THE TEAM COMBO BOX
    public void loadTeamsComboBox() {
        ObservableList<String> teamNames = FXCollections.observableArrayList();
        for (int i = 0;i < dataManager.getDraft().getTeams().size();i++) {
            teamNames.add(dataManager.getDraft().getTeams().get(i).getName());
        }
        fantasyTeamComboBox.setItems(teamNames);
    }
    
    public void initStartingLineupTableWorkspace() {
        startingLineupVBox = new VBox();
        startingLineupVBox.getStyleClass().add(CLASS_FANTASY_TEAMS);
        startingLineupLabel = new Label("STARTING LINEUP");
        startingLineupLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        startingLineupVBox.getChildren().add(startingLineupLabel);
        
        startingLineupTeamTable = new TableView();
        startingLineupPositionColumn = new TableColumn(COL_POSITION);
        startingLineupFirstColumn = new TableColumn(COL_FIRST);
        startingLineupLastColumn = new TableColumn(COL_LAST);
        startingLineupProTeamColumn = new TableColumn(COL_PRO_TEAM);
        startingLineupPositionsColumn = new TableColumn(COL_POSITIONS);
        startingLineupYobColumn = new TableColumn(COL_YOB);
        startingLineupRWColumn = new TableColumn(COL_R_W);
        startingLineupHrSvColumn = new TableColumn(COL_HR_SV);
        startingLineupRbiKColumn = new TableColumn(COL_RBI_K);
        startingLineupSbEraColumn = new TableColumn(COL_SB_ERA);
        startingLineupBaWhipColumn = new TableColumn(COL_BA_WHIP);
        startingLineupEstValueColumn = new TableColumn(COL_EST_VALUE);
        startingLineupNotesColumn = new TableColumn(COL_NOTES);
        startingLineupNotesColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        startingLineupNotesColumn.setEditable(true);
        startingLineupNotesColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<String, String>>() {
                @Override
                public void handle(CellEditEvent<String, String> t) {        
                    ((Player) t.getSource()).setNotes(t.getNewValue());
                }
            }
        );
        startingLineupPositionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("position"));
        startingLineupFirstColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        startingLineupLastColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        startingLineupProTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("team"));
        startingLineupPositionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("QP"));
        startingLineupYobColumn.setCellValueFactory(new PropertyValueFactory<String, String>("YoB"));
        startingLineupRWColumn.setCellValueFactory(new PropertyValueFactory<String, String>("RandW"));
        startingLineupHrSvColumn.setCellValueFactory(new PropertyValueFactory<String, String>("HRandSV"));
        startingLineupRbiKColumn.setCellValueFactory(new PropertyValueFactory<String, String>("RBIandK"));
        startingLineupSbEraColumn.setCellValueFactory(new PropertyValueFactory<String, String>("SBandERA"));
        startingLineupBaWhipColumn.setCellValueFactory(new PropertyValueFactory<String, String>("BAandWHIP"));
        startingLineupEstValueColumn.setCellValueFactory(new PropertyValueFactory<String, String>("estValue"));
        startingLineupNotesColumn.setCellValueFactory(new PropertyValueFactory<String, String>("notes"));
    
        startingLineupTeamTable.getColumns().add(startingLineupPositionColumn);
        startingLineupTeamTable.getColumns().add(startingLineupFirstColumn);
        startingLineupTeamTable.getColumns().add(startingLineupLastColumn);
        startingLineupTeamTable.getColumns().add(startingLineupProTeamColumn);
        startingLineupTeamTable.getColumns().add(startingLineupPositionsColumn);
        startingLineupTeamTable.getColumns().add(startingLineupYobColumn);
        startingLineupTeamTable.getColumns().add(startingLineupRWColumn);
        startingLineupTeamTable.getColumns().add(startingLineupHrSvColumn);
        startingLineupTeamTable.getColumns().add(startingLineupRbiKColumn);
        startingLineupTeamTable.getColumns().add(startingLineupSbEraColumn);
        startingLineupTeamTable.getColumns().add(startingLineupBaWhipColumn);
        startingLineupTeamTable.getColumns().add(startingLineupEstValueColumn);
        startingLineupTeamTable.getColumns().add(startingLineupNotesColumn);
        
        startingLineupVBox.getChildren().add(startingLineupTeamTable);
    }
    
    public void initTaxiSquadTableWorkspace() {
        taxiDraftVBox = new VBox();
        taxiDraftVBox.getStyleClass().add(CLASS_FANTASY_TEAMS);
        taxiDraftLabel = new Label("TAXI DRAFT");
        taxiDraftLabel.getStyleClass().add(CLASS_SUBHEADING_LABEL);
        taxiDraftVBox.getChildren().add(taxiDraftLabel);
        
        taxiDraftTeamTable = new TableView();
        taxiDraftPositionColumn = new TableColumn(COL_POSITION);
        taxiDraftFirstColumn = new TableColumn(COL_FIRST);
        taxiDraftLastColumn = new TableColumn(COL_LAST);
        taxiDraftProTeamColumn = new TableColumn(COL_PRO_TEAM);
        taxiDraftPositionsColumn = new TableColumn(COL_POSITIONS);
        taxiDraftYobColumn = new TableColumn(COL_YOB);
        taxiDraftRWColumn = new TableColumn(COL_R_W);
        taxiDraftHrSvColumn = new TableColumn(COL_HR_SV);
        taxiDraftRbiKColumn = new TableColumn(COL_RBI_K);
        taxiDraftSbEraColumn = new TableColumn(COL_SB_ERA);
        taxiDraftBaWhipColumn = new TableColumn(COL_BA_WHIP);
        taxiDraftEstValueColumn = new TableColumn(COL_EST_VALUE);
        taxiDraftNotesColumn = new TableColumn(COL_NOTES);
        startingLineupNotesColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        startingLineupNotesColumn.setEditable(true);
        startingLineupNotesColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<String, String>>() {
                @Override
                public void handle(CellEditEvent<String, String> t) {        
                    ((Player) t.getSource()).setNotes(t.getNewValue());
                }
            }
        );
        taxiDraftPositionColumn.setCellValueFactory(new PropertyValueFactory<String, String>("position"));
        taxiDraftFirstColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        taxiDraftLastColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        taxiDraftProTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("team"));
        taxiDraftPositionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("QP"));
        taxiDraftYobColumn.setCellValueFactory(new PropertyValueFactory<String, String>("YoB"));
        taxiDraftRWColumn.setCellValueFactory(new PropertyValueFactory<String, String>("RandW"));
        taxiDraftHrSvColumn.setCellValueFactory(new PropertyValueFactory<String, String>("HRandSV"));
        taxiDraftRbiKColumn.setCellValueFactory(new PropertyValueFactory<String, String>("RBIandK"));
        taxiDraftSbEraColumn.setCellValueFactory(new PropertyValueFactory<String, String>("SBandERA"));
        taxiDraftBaWhipColumn.setCellValueFactory(new PropertyValueFactory<String, String>("BAandWHIP"));
        taxiDraftEstValueColumn.setCellValueFactory(new PropertyValueFactory<String, String>("estValue"));
        taxiDraftNotesColumn.setCellValueFactory(new PropertyValueFactory<String, String>("notes"));
    
        taxiDraftTeamTable.getColumns().add(taxiDraftPositionColumn);
        taxiDraftTeamTable.getColumns().add(taxiDraftFirstColumn);
        taxiDraftTeamTable.getColumns().add(taxiDraftLastColumn);
        taxiDraftTeamTable.getColumns().add(taxiDraftProTeamColumn);
        taxiDraftTeamTable.getColumns().add(taxiDraftPositionsColumn);
        taxiDraftTeamTable.getColumns().add(taxiDraftYobColumn);
        taxiDraftTeamTable.getColumns().add(taxiDraftRWColumn);
        taxiDraftTeamTable.getColumns().add(taxiDraftHrSvColumn);
        taxiDraftTeamTable.getColumns().add(taxiDraftRbiKColumn);
        taxiDraftTeamTable.getColumns().add(taxiDraftSbEraColumn);
        taxiDraftTeamTable.getColumns().add(taxiDraftBaWhipColumn);
        taxiDraftTeamTable.getColumns().add(taxiDraftEstValueColumn);
        taxiDraftTeamTable.getColumns().add(taxiDraftNotesColumn);
        
        
        taxiDraftVBox.getChildren().add(taxiDraftTeamTable);
    }
    
    
    
    //ALL METHODS BELOW UNTIL NEXT SET OF METHODS ARE FOR FANTASY STANDINGS WORKSPACE
    private void initFantasyStandingsWorkspace() throws IOException {
        fantasyStandingsBorderPane = new BorderPane();
        fantasyStandingsScrollPane = new ScrollPane();
        fantasyStandingsWorkspaceBorderPane = new BorderPane();
        fantasyStandingsLabel = initLabel(WBDK_PropertyType.STANDINGS_HEADING_LABEL, CLASS_HEADING_LABEL);
        
        fantasyStandingsTable = new TableView();
         
        fantasyStandingsNameColumn = new TableColumn("Team Name");
        fantasyStandingsPlayersNeededColumn = new TableColumn("Players Needed");
        fantasyStandingsMoneyColumn = new TableColumn("$ Left");
        fantasyStandingsMoneyPerPlayerColumn = new TableColumn("$ PP");
        fantasyStandingsRColumn = new TableColumn("R");
        fantasyStandingsHRColumn = new TableColumn("HR");
        fantasyStandingsRBIColumn = new TableColumn("RBI");
        fantasyStandingsSBColumn = new TableColumn("SB");
        fantasyStandingsBAColumn = new TableColumn("BA");
        fantasyStandingsWColumn = new TableColumn("W");
        fantasyStandingsSVColumn = new TableColumn("SV");
        fantasyStandingsKColumn = new TableColumn("K");
        fantasyStandingsERAColumn = new TableColumn("ERA");
        fantasyStandingsWHIPColumn = new TableColumn("WHIP");
        fantasyStandingsTotalPointsColumn = new TableColumn("Total Points");
        fantasyStandingsNameColumn.setCellValueFactory(new PropertyValueFactory<String, String>("name"));
        fantasyStandingsPlayersNeededColumn.setCellValueFactory(new PropertyValueFactory<String, String>("playersNeeded"));
        fantasyStandingsMoneyColumn.setCellValueFactory(new PropertyValueFactory<String, String>("money"));
        fantasyStandingsMoneyPerPlayerColumn.setCellValueFactory(new PropertyValueFactory<String, String>("moneyPP"));
        fantasyStandingsRColumn.setCellValueFactory(new PropertyValueFactory<String, String>("teamR"));
        fantasyStandingsHRColumn.setCellValueFactory(new PropertyValueFactory<String, String>("teamHR"));
        fantasyStandingsRBIColumn.setCellValueFactory(new PropertyValueFactory<String, String>("teamRBI"));
        fantasyStandingsSBColumn.setCellValueFactory(new PropertyValueFactory<String, String>("teamSB"));
        fantasyStandingsBAColumn.setCellValueFactory(new PropertyValueFactory<String, String>("teamBA"));
        fantasyStandingsWColumn.setCellValueFactory(new PropertyValueFactory<String, String>("teamW"));
        fantasyStandingsSVColumn.setCellValueFactory(new PropertyValueFactory<String, String>("teamSV"));
        fantasyStandingsKColumn.setCellValueFactory(new PropertyValueFactory<String, String>("teamK"));
        fantasyStandingsERAColumn.setCellValueFactory(new PropertyValueFactory<String, String>("teamERA"));
        fantasyStandingsWHIPColumn.setCellValueFactory(new PropertyValueFactory<String, String>("teamWHIP"));
        fantasyStandingsTotalPointsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("totalPoints"));
        fantasyStandingsTable.getColumns().add(fantasyStandingsNameColumn);
        fantasyStandingsTable.getColumns().add(fantasyStandingsPlayersNeededColumn);
        fantasyStandingsTable.getColumns().add(fantasyStandingsMoneyColumn);
        fantasyStandingsTable.getColumns().add(fantasyStandingsMoneyPerPlayerColumn);
        fantasyStandingsTable.getColumns().add(fantasyStandingsRColumn);
        fantasyStandingsTable.getColumns().add(fantasyStandingsHRColumn);
        fantasyStandingsTable.getColumns().add(fantasyStandingsRBIColumn);
        fantasyStandingsTable.getColumns().add(fantasyStandingsSBColumn);
        fantasyStandingsTable.getColumns().add(fantasyStandingsBAColumn);
        fantasyStandingsTable.getColumns().add(fantasyStandingsWColumn);
        fantasyStandingsTable.getColumns().add(fantasyStandingsSVColumn);
        fantasyStandingsTable.getColumns().add(fantasyStandingsKColumn);
        fantasyStandingsTable.getColumns().add(fantasyStandingsERAColumn);
        fantasyStandingsTable.getColumns().add(fantasyStandingsWHIPColumn);
        fantasyStandingsTable.getColumns().add(fantasyStandingsTotalPointsColumn);
        fantasyStandingsTable.setItems(dataManager.getDraft().getTeams());
        
        fantasyStandingsWorkspaceBorderPane.setTop(fantasyStandingsLabel);
        fantasyStandingsWorkspaceBorderPane.setCenter(fantasyStandingsTable);
        fantasyStandingsScrollPane.setContent(fantasyStandingsWorkspaceBorderPane);
        fantasyStandingsScrollPane.setFitToWidth(true);
        fantasyStandingsScrollPane.setFitToHeight(true);
        fantasyStandingsBorderPane.setTop(fantasyStandingsFileToolbarPane);
        fantasyStandingsBorderPane.setCenter(fantasyStandingsScrollPane);
        fantasyStandingsBorderPane.setBottom(fantasyStandingsFantasyToolbarPane);
        fantasyStandingsWorkspaceBorderPane.getStyleClass().add(CLASS_FANTASY_STANDINGS);
        fantasyStandingsScrollPane.getStyleClass().add(CLASS_FANTASY_STANDINGS);
        fantasyStandingsBorderPane.getStyleClass().add(CLASS_FANTASY_STANDINGS);
        
    }
    
    //ALL METHODS BELOW UNTIL NEXT SET OF METHODS ARE FOR DRAFT SUMMARY WORKSPACE
    private void initDraftSummaryWorkspace() throws IOException {
        
        initDraftSummaryTopPane();
        initDraftSummaryTablePane();
        
        draftSummaryBorderPane = new BorderPane();
        draftSummaryScrollPane = new ScrollPane();
        draftSummaryWorkspaceBorderPane = new BorderPane();
        
        
        
        draftSummaryWorkspaceBorderPane.setTop(draftSummaryTopGridPane);
        draftSummaryWorkspaceBorderPane.setCenter(draftSummaryTable);
        draftSummaryScrollPane.setContent(draftSummaryWorkspaceBorderPane);
        draftSummaryScrollPane.setFitToHeight(true);
        draftSummaryScrollPane.setFitToWidth(true);
        draftSummaryBorderPane.setTop(draftSummaryFileToolbarPane);
        draftSummaryBorderPane.setCenter(draftSummaryScrollPane);
        draftSummaryBorderPane.setBottom(draftSummaryFantasyToolbarPane);
        draftSummaryTopGridPane.getStyleClass().add(CLASS_DRAFT_SUMMARY);
        draftSummaryWorkspaceBorderPane.getStyleClass().add(CLASS_DRAFT_SUMMARY);
        draftSummaryScrollPane.getStyleClass().add(CLASS_DRAFT_SUMMARY);
        draftSummaryBorderPane.getStyleClass().add(CLASS_DRAFT_SUMMARY);
        
    }
    
    private void initDraftSummaryTopPane() {
        draftSummaryTopGridPane = new GridPane();
        draftSummaryLabel = initLabel(WBDK_PropertyType.DRAFT_SUMMARY_HEADING_LABEL, CLASS_HEADING_LABEL);
        draftSummaryStarButton = initChildButton(WBDK_PropertyType.STAR_BUTTON, WBDK_PropertyType.STAR_TOOLTIP, false);
        draftSummaryStartDraftButton = initChildButton(WBDK_PropertyType.START_BUTTON, WBDK_PropertyType.START_TOOLTIP, false);
        draftSummaryStopDraftButton = initChildButton(WBDK_PropertyType.STOP_BUTTON, WBDK_PropertyType.STOP_TOOLTIP, false);
        
        draftSummaryTopGridPane.add(draftSummaryLabel, 0, 0, 1, 1);
        draftSummaryTopGridPane.add(draftSummaryStarButton, 0, 1, 1, 1);
        draftSummaryTopGridPane.add(draftSummaryStartDraftButton, 1, 1, 1, 1);
        draftSummaryTopGridPane.add(draftSummaryStopDraftButton, 2, 1, 1, 1);
        
    }
    
    private void initDraftSummaryTablePane() {
        draftSummaryTable = new TableView();
        draftSummaryPickNumberColumn = new TableColumn("Pick #");
        draftSummaryFirstColumn = new TableColumn("First");
        draftSummaryLastColumn = new TableColumn("Last");
        draftSummaryTeamColumn = new TableColumn("Team");
        draftSummaryContractColumn = new TableColumn("Contract");
        draftSummarySalaryColumn = new TableColumn("Salary");
        draftSummaryPickNumberColumn.setCellValueFactory(new PropertyValueFactory<String, String>("pickNumber"));
        draftSummaryFirstColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        draftSummaryLastColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        draftSummaryTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("teamName"));
        draftSummaryContractColumn.setCellValueFactory(new PropertyValueFactory<String, String>("contract"));
        draftSummarySalaryColumn.setCellValueFactory(new PropertyValueFactory<String, String>("salary"));
        draftSummaryTable.getColumns().add(draftSummaryPickNumberColumn);
        draftSummaryTable.getColumns().add(draftSummaryFirstColumn);
        draftSummaryTable.getColumns().add(draftSummaryLastColumn);
        draftSummaryTable.getColumns().add(draftSummaryTeamColumn);
        draftSummaryTable.getColumns().add(draftSummaryContractColumn);
        draftSummaryTable.getColumns().add(draftSummarySalaryColumn);
        draftSummaryTable.setItems(dataManager.getDraft().getDraftPicks());
        
        
        
        
    }
    
    //ALL METHODS BELOW UNTIL NEXT SET OF METHODS ARE FOR MLB TEAMS WORKSPACE
    private void initMLBTeamsWorkspace() throws IOException {
        MLBTeamsBorderPane = new BorderPane();
        MLBTeamsScrollPane = new ScrollPane();
        MLBTeamsWorkspaceBorderPane = new BorderPane();
        MLBTeamsLabel = initLabel(WBDK_PropertyType.MLB_TEAMS_HEADING_LABEL, CLASS_HEADING_LABEL);
        
        
        MLBTeamsControlGridPane = new GridPane();
        MLBTeamsComboBox = new ComboBox();
        MLBTeamsSelectLabel = new Label("MLB Team: ");
        MLBTeamsSelectLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        MLBTeamsComboBox.getItems().addAll("All", "ATL", "AZ", "CHC", "CIN", "COL", "LAD", "MIA", "MIL", "NYM", "PHI", "PIT", "SD", "SF", "STL", "TOR", "WSH");
        MLBTeamsControlGridPane.add(MLBTeamsLabel, 0, 0, 2, 1);
        MLBTeamsControlGridPane.add(MLBTeamsSelectLabel, 0, 1, 1, 1);
        MLBTeamsControlGridPane.add(MLBTeamsComboBox, 1, 1, 1, 1);
        
        MLBTeamsTable = new TableView();
        MLBTeamsFirstColumn = new TableColumn(COL_FIRST);
        MLBTeamsLastColumn = new TableColumn(COL_LAST);
        MLBTeamsProTeamColumn = new TableColumn(COL_PRO_TEAM);
        MLBTeamsPositionsColumn = new TableColumn(COL_POSITIONS);
        MLBTeamsYobColumn = new TableColumn(COL_YOB);
        MLBTeamsRWColumn = new TableColumn(COL_R_W);
        MLBTeamsHrSvColumn = new TableColumn(COL_HR_SV);
        MLBTeamsRbiKColumn = new TableColumn(COL_RBI_K);
        MLBTeamsSbEraColumn = new TableColumn(COL_SB_ERA);
        MLBTeamsBaWhipColumn = new TableColumn(COL_BA_WHIP);
        MLBTeamsEstValueColumn = new TableColumn(COL_EST_VALUE);
        MLBTeamsFirstColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        MLBTeamsLastColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        MLBTeamsProTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("team"));
        MLBTeamsPositionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("QP"));
        MLBTeamsYobColumn.setCellValueFactory(new PropertyValueFactory<String, String>("YoB"));
        MLBTeamsRWColumn.setCellValueFactory(new PropertyValueFactory<String, String>("RandW"));
        MLBTeamsHrSvColumn.setCellValueFactory(new PropertyValueFactory<String, String>("HRandSV"));
        MLBTeamsRbiKColumn.setCellValueFactory(new PropertyValueFactory<String, String>("RBIandK"));
        MLBTeamsSbEraColumn.setCellValueFactory(new PropertyValueFactory<String, String>("SBandERA"));
        MLBTeamsBaWhipColumn.setCellValueFactory(new PropertyValueFactory<String, String>("BAandWHIP"));
        MLBTeamsEstValueColumn.setCellValueFactory(new PropertyValueFactory<String, String>("estValue"));
        MLBTeamsTable.getColumns().add(MLBTeamsFirstColumn);
        MLBTeamsTable.getColumns().add(MLBTeamsLastColumn);
        MLBTeamsTable.getColumns().add(MLBTeamsProTeamColumn);
        MLBTeamsTable.getColumns().add(MLBTeamsPositionsColumn);
        MLBTeamsTable.getColumns().add(MLBTeamsYobColumn);
        MLBTeamsTable.getColumns().add(MLBTeamsRWColumn);
        MLBTeamsTable.getColumns().add(MLBTeamsHrSvColumn);
        MLBTeamsTable.getColumns().add(MLBTeamsRbiKColumn);
        MLBTeamsTable.getColumns().add(MLBTeamsSbEraColumn);
        MLBTeamsTable.getColumns().add(MLBTeamsBaWhipColumn);
        MLBTeamsTable.getColumns().add(MLBTeamsEstValueColumn);
        MLBTeamsTable.setItems(dataManager.getDraft().getPlayers());
        
        
        MLBTeamsWorkspaceBorderPane.setTop(MLBTeamsControlGridPane);
        MLBTeamsWorkspaceBorderPane.setCenter(MLBTeamsTable);
        MLBTeamsScrollPane.setContent(MLBTeamsWorkspaceBorderPane);
        MLBTeamsScrollPane.setFitToWidth(true);
        MLBTeamsScrollPane.setFitToHeight(true);
        MLBTeamsBorderPane.setTop(MLBTeamsFileToolbarPane);
        MLBTeamsBorderPane.setCenter(MLBTeamsScrollPane);
        MLBTeamsBorderPane.setBottom(MLBTeamsFantasyToolbarPane);
        
        MLBTeamsWorkspaceBorderPane.getStyleClass().add(CLASS_MLB_TEAMS);
        MLBTeamsScrollPane.getStyleClass().add(CLASS_MLB_TEAMS);
        MLBTeamsBorderPane.getStyleClass().add(CLASS_MLB_TEAMS);
    }
    
    private void initInitialWorkspace() throws IOException {
        initialBorderPane = new BorderPane();
        initialBorderPane.setTop(initialFileToolbarPane);
    }
    
    //ALL METHODS BELOW UNTIL NEXT SET OF METHODS ARE FOR AVAILABLE PLAYERS
    private void initAvailablePlayersWorkspace() throws IOException {
        
        //THE SEARCH BAR WORKSPACE IS ON TOP AND HAS THE SEARCH BAR, ADD AND REMOVE BUTTONS, AND THE AVAILABLE PLAYERS LABEL
        initAvailablePlayersSearchBarWorkspace();
        
        //THE RADIO BUTTONS WORKSPACE HAS ALL OF THE RADIO BUTTONS FOR REDUCING THE LIST
        initAvailablePlayersButtonsWorkspace();
        
        initAvailablePlayersPlayersList();
        
        // THE WORKSPACE HAS A FEW REGIONS, THIS 
        // IS FOR BASIC COURSE EDITING CONTROLS
        availablePlayersScrollPane = new ScrollPane();
        availablePlayersScrollPane.setFitToWidth(true);
        availablePlayersScrollPane.setFitToHeight(true);
        availablePlayersBorderPane = new BorderPane();
        availablePlayersWorkspaceBorderPane = new BorderPane();
        
        //SET UP THE WHOLE SCENE
        availablePlayersWorkspaceBorderPane.setTop(playerListControlsFlowPane);
        availablePlayersWorkspaceBorderPane.setCenter(playersList);
        availablePlayersWorkspaceBorderPane.setBottom(searchButtonsHBox);
        availablePlayersScrollPane.setContent(availablePlayersWorkspaceBorderPane);
        availablePlayersScrollPane.setFitToWidth(true);
        availablePlayersBorderPane.setTop(availablePlayersFileToolbarPane);
        availablePlayersBorderPane.setCenter(availablePlayersScrollPane);
        availablePlayersBorderPane.setBottom(availablePlayersFantasyToolbarPane);
        availablePlayersWorkspaceBorderPane.getStyleClass().add(CLASS_AVAILABLE_PLAYERS);
        availablePlayersScrollPane.getStyleClass().add(CLASS_AVAILABLE_PLAYERS);
        availablePlayersBorderPane.getStyleClass().add(CLASS_AVAILABLE_PLAYERS);
    }

    private void initAvailablePlayersSearchBarWorkspace() {
        playerListControlsFlowPane = new FlowPane();
        availablePlayersLabel = initLabel(WBDK_PropertyType.AVAILABLE_PLAYERS_HEADING_LABEL, CLASS_HEADING_LABEL);
        addPlayerButton = initChildButton(WBDK_PropertyType.ADD_ICON, WBDK_PropertyType.ADD_PLAYER_TOOLTIP, false);
        removePlayerButton = initChildButton(WBDK_PropertyType.MINUS_ICON, WBDK_PropertyType.REMOVE_PLAYER_TOOLTIP, false);
        searchLabel = new Label("Search:");
        searchTextField = new TextField();
        playerListControlsFlowPane.setPadding(new Insets(11, 12, 13, 14));
        playerListControlsFlowPane.setHgap(5);
        playerListControlsFlowPane.setVgap(5);
        
        playerListControlsFlowPane.getChildren().addAll(availablePlayersLabel);
        playerListControlsFlowPane.getChildren().addAll(addPlayerButton, removePlayerButton, searchLabel, searchTextField);
        

    }
    
    private void initAvailablePlayersButtonsWorkspace() {
        searchButtonsHBox = new HBox();
        searchButtonsHBox.setPadding(new Insets(10,10,10,10));
        sortLabel = new Label("Sort By: ");
        sortLabel.getStyleClass().add(CLASS_PROMPT_LABEL);
        allButtonLabel = new Label("   All");
        allButton = new Button("All");
        catcherButtonLabel = new Label("   C");
        catcherButton = new Button("C");
        firstBaseButtonLabel = new Label("   1B");
        firstBaseButton = new Button("1B");
        cornerInfielderButtonLabel = new Label("   CI");
        cornerInfielderButton = new Button("CI");
        thirdBaseButtonLabel = new Label("   3B");
        thirdBaseButton = new Button("3B");
        secondBaseButtonLabel = new Label("   2B");
        secondBaseButton = new Button("2B");
        middleInfielderbuttonLabel = new Label("   MI");
        middleInfielderButton = new Button("MI");
        shortStopButtonLabel = new Label("   SS");
        shortStopButton = new Button("SS");
        outfielderButtonLabel = new Label("   OF");
        outfielderButton = new Button("OF");
        utilitybuttonLabel = new Label("   U");
        utilityButton = new Button("U");
        pitcherButtonLabel = new Label("   P");
        pitcherButton = new Button("P");
        
        searchButtonsHBox.getChildren().add(sortLabel);
        searchButtonsHBox.getChildren().add(allButton);
        searchButtonsHBox.getChildren().add(catcherButton);
        searchButtonsHBox.getChildren().add(firstBaseButton);
        searchButtonsHBox.getChildren().add(cornerInfielderButton);
        searchButtonsHBox.getChildren().add(thirdBaseButton);
        searchButtonsHBox.getChildren().add(secondBaseButton);
        searchButtonsHBox.getChildren().add(shortStopButton);
        searchButtonsHBox.getChildren().add(middleInfielderButton);
        searchButtonsHBox.getChildren().add(outfielderButton);
        searchButtonsHBox.getChildren().add(utilityButton);
        searchButtonsHBox.getChildren().add(pitcherButton);
    }
    
    private void initAvailablePlayersPlayersList() { 
        playersList = new TableView();
        tableFlowPane = new FlowPane();
        
        firstColumn = new TableColumn(COL_FIRST);
        lastColumn = new TableColumn(COL_LAST);
        proTeamColumn = new TableColumn(COL_PRO_TEAM);
        positionsColumn = new TableColumn(COL_POSITIONS);
        yobColumn = new TableColumn(COL_YOB);
        rWColumn = new TableColumn(COL_R_W);
        hrSvColumn = new TableColumn(COL_HR_SV);
        rbiKColumn = new TableColumn(COL_RBI_K);
        sbEraColumn = new TableColumn(COL_SB_ERA);
        baWhipColumn = new TableColumn(COL_BA_WHIP);
        estValueColumn = new TableColumn(COL_EST_VALUE);
        notesColumn = new TableColumn(COL_NOTES);
        notesColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        notesColumn.setEditable(true);
        notesColumn.setOnEditCommit(
            new EventHandler<CellEditEvent<String, String>>() {
                @Override
                public void handle(CellEditEvent<String, String> t) {        
                    ((Player) t.getSource()).setNotes(t.getNewValue());
                }
            }
        );
        
        firstColumn.setCellValueFactory(new PropertyValueFactory<String, String>("firstName"));
        lastColumn.setCellValueFactory(new PropertyValueFactory<String, String>("lastName"));
        proTeamColumn.setCellValueFactory(new PropertyValueFactory<String, String>("team"));
        positionsColumn.setCellValueFactory(new PropertyValueFactory<String, String>("QP"));
        yobColumn.setCellValueFactory(new PropertyValueFactory<String, String>("YoB"));
        rWColumn.setCellValueFactory(new PropertyValueFactory<String, String>("RandW"));
        hrSvColumn.setCellValueFactory(new PropertyValueFactory<String, String>("HRandSV"));
        rbiKColumn.setCellValueFactory(new PropertyValueFactory<String, String>("RBIandK"));
        sbEraColumn.setCellValueFactory(new PropertyValueFactory<String, String>("SBandERA"));
        baWhipColumn.setCellValueFactory(new PropertyValueFactory<String, String>("BAandWHIP"));
        estValueColumn.setCellValueFactory(new PropertyValueFactory<String, String>("estValue"));
        notesColumn.setCellValueFactory(new PropertyValueFactory<String, String>("notes"));
        
        playersList.getColumns().add(firstColumn);
        playersList.getColumns().add(lastColumn);
        playersList.getColumns().add(proTeamColumn);
        playersList.getColumns().add(positionsColumn);
        playersList.getColumns().add(yobColumn);
        playersList.getColumns().add(rWColumn);
        playersList.getColumns().add(hrSvColumn);
        playersList.getColumns().add(rbiKColumn);
        playersList.getColumns().add(sbEraColumn);
        playersList.getColumns().add(baWhipColumn);
        playersList.getColumns().add(estValueColumn);
        playersList.getColumns().add(notesColumn);
        
        playersList.setItems(((dataManager.getDraft()).getPlayers()));
    }
    
    

    private Button initChildButton(WBDK_PropertyType icon, WBDK_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        return button;
    }
    
    private Label initLabel(WBDK_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText.replace('_', ' '));
        label.getStyleClass().add(styleClass);
        return label;
    }
    
    
    private void initEventHandlers() throws IOException {
        //fileController = new FileController(messageDialog, yesNoCancelDialog, draftFileManager, siteExporter);
        
        //FIRST, THE FILE CONTROLS
        initialNewDraftButton.setOnAction(e -> {
            //fileController.hangleNewDraftRequest(this);
            loadScene(1);
            messageDialog.show(properties.getProperty(NEW_DRAFT_CREATED_MESSAGE));
        });
        /*
        initialLoadDraftButton.setOnAction(e -> {
            fileController.handleLoadDraftRequest(this);
        });
        
        
        initialSaveDraftButton.setOnAction(e -> {
            fileController.handleSaveDraftRequest(this, dataManager.getDraft());
        });
        */
        /*
        initialExportSiteButton.setOnAction(e -> {
            fileController.handleExportCourseRequest(this);
        });
        */
        initialExitButton.setOnAction(e -> {
            fileController.handleExitRequest();
        });
        
        fantasyTeamsNewDraftButton.setOnAction(e -> {
            //fileController.hangleNewDraftRequest(this);
            loadScene(1);
            messageDialog.show(properties.getProperty("New Draft Created but Not Saved"));
        });
        /*
        fantasyTeamsLoadDraftButton.setOnAction(e -> {
            fileController.handleLoadDraftRequest(this);
        });
        
        
        fantasyTeamsSaveDraftButton.setOnAction(e -> {
            fileController.handleSaveDraftRequest(this, dataManager.getDraft());
        });
        */
        /*
        fantasyTeamsExportSiteButton.setOnAction(e -> {
            fileController.handleExportCourseRequest(this);
        });
        */
        fantasyTeamsExitButton.setOnAction(e -> {
            fileController.handleExitRequest();
        });
        availablePlayersNewDraftButton.setOnAction(e -> {
            //fileController.hangleNewDraftRequest(this);
            loadScene(1);
            messageDialog.show(properties.getProperty(NEW_DRAFT_CREATED_MESSAGE));
        });
        /*
        availablePlayersLoadDraftButton.setOnAction(e -> {
            fileController.handleLoadDraftRequest(this);
        });
        
        
        availablePlayersSaveDraftButton.setOnAction(e -> {
            fileController.handleSaveDraftRequest(this, dataManager.getDraft());
        });
        */
        /*
        availablePlayersExportSiteButton.setOnAction(e -> {
            fileController.handleExportCourseRequest(this);
        });
        */
        availablePlayersExitButton.setOnAction(e -> {
            fileController.handleExitRequest();
        });
        fantasyStandingsNewDraftButton.setOnAction(e -> {
            //fileController.hangleNewDraftRequest(this);
            loadScene(1);
            messageDialog.show(properties.getProperty("New Draft Created but Not Saved"));
        });
        /*
        fantasyStandingsLoadDraftButton.setOnAction(e -> {
            fileController.handleLoadDraftRequest(this);
        });
        
        
        fantasyStandingsSaveDraftButton.setOnAction(e -> {
            fileController.handleSaveDraftRequest(this, dataManager.getDraft());
        });
        */
        /*
        fantasyStandingsExportSiteButton.setOnAction(e -> {
            fileController.handleExportCourseRequest(this);
        });
        */
        fantasyStandingsExitButton.setOnAction(e -> {
            fileController.handleExitRequest();
        });
        draftSummaryNewDraftButton.setOnAction(e -> {
            //fileController.hangleNewDraftRequest(this);
            loadScene(1);
            messageDialog.show(properties.getProperty("New Draft Created but Not Saved"));
        });
        /*
        draftSummaryLoadDraftButton.setOnAction(e -> {
            fileController.handleLoadDraftRequest(this);
        });
        
        
        draftSummarySaveDraftButton.setOnAction(e -> {
            fileController.handleSaveDraftRequest(this, dataManager.getDraft());
        });
        */
        /*
        draftSummaryExportSiteButton.setOnAction(e -> {
            fileController.handleExportCourseRequest(this);
        });
        */
        draftSummaryExitButton.setOnAction(e -> {
            fileController.handleExitRequest();
        });
        MLBTeamsNewDraftButton.setOnAction(e -> {
            //fileController.hangleNewDraftRequest(this);
            loadScene(1);
            messageDialog.show(properties.getProperty("New Draft Created but Not Saved"));
        });
        /*
        MLBTeamsLoadDraftButton.setOnAction(e -> {
            fileController.handleLoadDraftRequest(this);
        });
        
        
        MLBTeamsSaveDraftButton.setOnAction(e -> {
            fileController.handleSaveDraftRequest(this, dataManager.getDraft());
        });
        */
        /*
        MLBTeamsExportSiteButton.setOnAction(e -> {
            fileController.handleExportCourseRequest(this);
        });
        */
        MLBTeamsExitButton.setOnAction(e -> {
            fileController.handleExitRequest();
        });
        
        
        //NEXT, THE FANTASY CONTROLS
        fantasyTeamsFantasyTeamsButton.setOnAction(e -> {
            loadScene(1);
        });
        fantasyTeamsAvailablePlayersButton.setOnAction(e -> {
            loadScene(2);
        });
        fantasyTeamsFantasyStandingsButton.setOnAction(e -> {
            loadScene(3);
        });
        fantasyTeamsDraftSummaryButton.setOnAction(e -> {
            loadScene(4);
        });
        fantasyTeamsMLBTeamsButton.setOnAction(e -> {
            loadScene(5);
        });
        availablePlayersFantasyTeamsButton.setOnAction(e -> {
            loadScene(1);
        });
        availablePlayersAvailablePlayersButton.setOnAction(e -> {
            loadScene(2);
        });
        availablePlayersFantasyStandingsButton.setOnAction(e -> {
            loadScene(3);
        });
        availablePlayersDraftSummaryButton.setOnAction(e -> {
            loadScene(4);
        });
        availablePlayersMLBTeamsButton.setOnAction(e -> {
            loadScene(5);
        });
        fantasyStandingsFantasyTeamsButton.setOnAction(e -> {
            loadScene(1);
        });
        fantasyStandingsAvailablePlayersButton.setOnAction(e -> {
            loadScene(2);
        });
        fantasyStandingsFantasyStandingsButton.setOnAction(e -> {
            loadScene(3);
        });
        fantasyStandingsDraftSummaryButton.setOnAction(e -> {
            loadScene(4);
        });
        fantasyStandingsMLBTeamsButton.setOnAction(e -> {
            loadScene(5);
        });
        draftSummaryFantasyTeamsButton.setOnAction(e -> {
            loadScene(1);
        });
        draftSummaryAvailablePlayersButton.setOnAction(e -> {
            loadScene(2);
        });
        draftSummaryFantasyStandingsButton.setOnAction(e -> {
            loadScene(3);
        });
        draftSummaryDraftSummaryButton.setOnAction(e -> {
            loadScene(4);
        });
        draftSummaryMLBTeamsButton.setOnAction(e -> {
            loadScene(5);
        });
        MLBTeamsFantasyTeamsButton.setOnAction(e -> {
            loadScene(1);
        });
        MLBTeamsAvailablePlayersButton.setOnAction(e -> {
            loadScene(2);
        });
        MLBTeamsFantasyStandingsButton.setOnAction(e -> {
            loadScene(3);
        });
        MLBTeamsDraftSummaryButton.setOnAction(e -> {
            loadScene(4);
        });
        MLBTeamsMLBTeamsButton.setOnAction(e -> {
            loadScene(5);
        });
        
        
        //SEARCH BUTTONS IN AVAILABLE PLAYERS
        sortTextField(searchTextField);
        
        allButton.setOnAction(e -> {
           sortPlayers("all"); 
        });
        catcherButton.setOnAction(e -> {
           sortPlayers("C"); 
        });
        firstBaseButton.setOnAction(e -> {
           sortPlayers("1B"); 
        });
        cornerInfielderButton.setOnAction(e -> {
           sortPlayers("CI"); 
        });
        thirdBaseButton.setOnAction(e -> {
           sortPlayers("3B"); 
        });
        secondBaseButton.setOnAction(e -> {
           sortPlayers("2B"); 
        });
        middleInfielderButton.setOnAction(e -> {
           sortPlayers("MI"); 
        });
        shortStopButton.setOnAction(e -> {
           sortPlayers("SS"); 
        });
        outfielderButton.setOnAction(e -> {
           sortPlayers("OF"); 
        });
        utilityButton.setOnAction(e -> {
           sortPlayers("U"); 
        });
        pitcherButton.setOnAction(e -> {
           sortPlayers("P"); 
        });
        
        //PLAYER ADDING AND EDITING CONTROLS IN AVAILABLE PLAYERS
        draftController = new DraftEditController(primaryStage, messageDialog, yesNoCancelDialog);
        addPlayerButton.setOnAction(e -> {
            draftController.handleAddPlayerRequest(((dataManager.getDraft()).getPlayers()));
            dataManager.getDraft().calculateEstimatedValues();
        });
        removePlayerButton.setOnAction(e -> {
            draftController.handleRemovePlayerRequest(this, playersList.getSelectionModel().getSelectedItem());
            dataManager.getDraft().calculateEstimatedValues();
        });
        
        //DRAFT NAME FOR SAVING
        registerTextFieldController(draftNameTextField);
        
        //ADD, REMOVE, AND EDIT TEAMS BUTTON
        addTeamButton.setOnAction(e -> {
            draftController.handleAddTeamRequest(this);
            loadTeamsComboBox();
            dataManager.getDraft().setPoints();
            fantasyStandingsTable.setItems(dataManager.getDraft().getTeams());
            System.out.println();dataManager.getDraft().getTeams().get(0).getStartingPlayers().size();
        });
        removeTeamButton.setOnAction(e -> {
            Team teamToRemove = new Team();
            for (int i = 0;i < ((dataManager.getDraft()).getTeams()).size();i++) {
                if ((fantasyTeamComboBox.getSelectionModel().getSelectedItem()).equals(((dataManager.getDraft()).getTeams()).get(i).getName())) {
                    
                    teamToRemove = ((dataManager.getDraft()).getTeams()).get(i);
                    
                    break;
                }
            }
            draftController.handleRemoveTeamRequest(this, teamToRemove);
            loadTeamsComboBox();
            dataManager.getDraft().setPoints();
            for (int i = 0;i < dataManager.getDraft().getTeams().size();i++) {
                dataManager.getDraft().getTeams().get(i).updateInfo();
            }
            fantasyStandingsTable.setItems(dataManager.getDraft().getTeams());
        });
        editTeamButton.setOnAction(e -> {
            Team teamToEdit = new Team();
            for (int i = 0;i < ((dataManager.getDraft()).getTeams()).size();i++) {
                if ((fantasyTeamComboBox.getSelectionModel().getSelectedItem()).equals(((dataManager.getDraft()).getTeams()).get(i).getName())) {
                    teamToEdit = ((dataManager.getDraft()).getTeams()).get(i);
                    break;
                }
            }
            draftController.handleEditTeamRequest(this, teamToEdit);
            loadTeamsComboBox();
            
        });
        
        //COMBO BOX FOR SELECTING TEAMS
        fantasyTeamComboBox.setOnAction(e -> {
            ObservableList<Player> players = FXCollections.observableArrayList();
            for (int i = 0;i < dataManager.getDraft().getTeams().size();i++) {
                if (fantasyTeamComboBox.getSelectionModel().getSelectedItem().equals(dataManager.getDraft().getTeams().get(i).getName())) {
                    players = dataManager.getDraft().getTeams().get(i).getStartingPlayers();
                }
            }
            startingLineupTeamTable.setItems(players);
            
            players = FXCollections.observableArrayList();
            for (int i = 0;i < dataManager.getDraft().getTeams().size();i++) {
                if (fantasyTeamComboBox.getSelectionModel().getSelectedItem().equals(dataManager.getDraft().getTeams().get(i).getName())) {
                    players = dataManager.getDraft().getTeams().get(i).getTaxiPlayers();
                }
            }
            taxiDraftTeamTable.setItems(players);
        });
        
        //EDITING PLAYERS IN AVAILABLE PLAYERS
        playersList.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                // OPEN UP THE SCHEDULE ITEM EDITOR
                Player p = playersList.getSelectionModel().getSelectedItem();
                draftController.handleEditPlayerRequest(this, p, true, false, new Team());
                fantasyStandingsTable.setItems(dataManager.getDraft().getTeams());
            }
            for (int i = 0;i < dataManager.getDraft().getTeams().size();i++) {
                dataManager.getDraft().getTeams().get(i).updateInfo();
            }
            dataManager.getDraft().setPoints();
            dataManager.getDraft().calculateEstimatedValues();
        });
        
        taxiDraftTeamTable.setOnMouseClicked(e -> {
            if (e.getClickCount() == 2) {
                Team teamSelected = new Team();
                if (dataManager.getDraft().getTeams().size() > 0) {
                    for (int i = 0;i < dataManager.getDraft().getTeams().size();i++) {
                        if (dataManager.getDraft().getTeams().get(i).getName().equals(fantasyTeamComboBox.getSelectionModel().getSelectedItem())) {
                            teamSelected = dataManager.getDraft().getTeams().get(i);
                            break;
                        }
                    }
                }
                // OPEN UP THE SCHEDULE ITEM EDITOR
                Player p = taxiDraftTeamTable.getSelectionModel().getSelectedItem();
                String pos = p.getPosition();
                draftController.handleEditPlayerRequest(this, p, false, true, teamSelected);
                if (!(p.getPosition().equals(pos))) {
                    
                }
                else if (p.getPosition().equals("null")) {
                    dataManager.getDraft().getPlayers().add(p);
                }
                for (int i = 0;i < dataManager.getDraft().getTeams().size();i++) {
                    dataManager.getDraft().getTeams().get(i).updateInfo();
                }
                dataManager.getDraft().setPoints();
                fantasyStandingsTable.setItems(dataManager.getDraft().getTeams());
                dataManager.getDraft().calculateEstimatedValues();
            }
        });
        
        //EDITING PLAYERS IN FANTASY TEAMS
        startingLineupTeamTable.setOnMouseClicked(e -> {
            
            if (e.getClickCount() == 2) {
                Team teamSelected = new Team();
                if (dataManager.getDraft().getTeams().size() > 0) {
                    for (int i = 0;i < dataManager.getDraft().getTeams().size();i++) {
                        if (dataManager.getDraft().getTeams().get(i).getName().equals(fantasyTeamComboBox.getSelectionModel().getSelectedItem())) {
                            teamSelected = dataManager.getDraft().getTeams().get(i);
                            break;
                        }
                    }
                }
                // OPEN UP THE SCHEDULE ITEM EDITOR
                Player p = startingLineupTeamTable.getSelectionModel().getSelectedItem();
                String pos = p.getPosition();
                draftController.handleEditPlayerRequest(this, p, false, false, teamSelected);
                if (!(p.getPosition().equals(pos))) {
                    
                }
                else if (p.getPosition().equals("null")) {
                    dataManager.getDraft().getPlayers().add(p);
                }
                for (int i = 0;i < dataManager.getDraft().getTeams().size();i++) {
                    dataManager.getDraft().getTeams().get(i).updateInfo();
                }
                dataManager.getDraft().setPoints();
                fantasyStandingsTable.setItems(dataManager.getDraft().getTeams());
                dataManager.getDraft().calculateEstimatedValues();
            }
        });
        
        //EDITING THE LIST IN MLB TEAMS SCENE
        MLBTeamsComboBox.setOnAction(e -> {
            ObservableList<Player> mlbTeam = FXCollections.observableArrayList();
            if (MLBTeamsComboBox.getSelectionModel().getSelectedItem().equals("All")) {
                MLBTeamsTable.setItems(dataManager.getDraft().getPlayers());
            }
            else {
                for (int i = 0;i < dataManager.getDraft().getPlayers().size();i++) {
                    if (dataManager.getDraft().getPlayers().get(i).getTeam().equals(MLBTeamsComboBox.getSelectionModel().getSelectedItem())) {
                        mlbTeam.add(dataManager.getDraft().getPlayers().get(i));
                    }
                }
                MLBTeamsTable.setItems(mlbTeam);
            }
        });
        
        //AUTO DRAFT
        
        draftSummaryStarButton.setOnAction(e -> {
            autoDraftPlayer();
            dataManager.getDraft().setPoints();
            dataManager.getDraft().calculateEstimatedValues();
        });
        draftSummaryStartDraftButton.setOnAction(e -> {
            runningAutoDraft = true;
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        while (runningAutoDraft) {
                            autoDraftPlayer();
                            for (int i = 0;i < dataManager.getDraft().getTeams().size();i++) {
                                dataManager.getDraft().getTeams().get(i).updateInfo();
                            }
                            dataManager.getDraft().setPoints();
                            dataManager.getDraft().calculateEstimatedValues();
                            Thread.sleep(2000);
                        }
                    } catch (Exception e) {}
                }
            }).start();
        });
        draftSummaryStopDraftButton.setOnAction(e -> {
            runningAutoDraft = false;
        });
    }
    public void autoDraftPlayer() {
        if (dataManager.getDraft().getTeams().size() > 0) {
                StringBuilder pos = new StringBuilder();
                boolean breakNow = false;
                boolean taxi = false;
                int[] positionCount = new int[10];
                int i = 0;
                int k = 0;
                int j = 0;
                for (i = 0;i < dataManager.getDraft().getTeams().size();i++) {
                    if (dataManager.getDraft().getTeams().get(i).getStartingPlayers().size() < 14) {
                        break;
                    }
                    else if (dataManager.getDraft().getTeams().get(i).getTaxiPlayers().size() < 8) {
                        break;
                    }
                }
                for (k = 0;k < dataManager.getDraft().getTeams().get(i).getStartingPlayers().size();k++) {
                    if ((dataManager.getDraft().getTeams().get(i).getStartingPlayers().get(k).getPosition()).equals("C")) {
                        positionCount[0]++;
                    }
                    else if ((dataManager.getDraft().getTeams().get(i).getStartingPlayers().get(k).getPosition()).equals("1B")) {
                        positionCount[1]++;
                    }
                    else if ((dataManager.getDraft().getTeams().get(i).getStartingPlayers().get(k).getPosition()).equals("CI")) {
                        positionCount[2]++;
                    }
                    else if ((dataManager.getDraft().getTeams().get(i).getStartingPlayers().get(k).getPosition()).equals("3B")) {
                        positionCount[3]++;
                    }
                    else if ((dataManager.getDraft().getTeams().get(i).getStartingPlayers().get(k).getPosition()).equals("2B")) {
                        positionCount[4]++;
                    }
                    else if ((dataManager.getDraft().getTeams().get(i).getStartingPlayers().get(k).getPosition()).equals("MI")) {
                        positionCount[5]++;
                    }
                    else if ((dataManager.getDraft().getTeams().get(i).getStartingPlayers().get(k).getPosition()).equals("SS")) {
                        positionCount[6]++;
                    }
                    else if ((dataManager.getDraft().getTeams().get(i).getStartingPlayers().get(k).getPosition()).equals("OF")) {
                        positionCount[7]++;
                    }
                    else if ((dataManager.getDraft().getTeams().get(i).getStartingPlayers().get(k).getPosition()).equals("P")) {
                        positionCount[8]++;
                    }
                    else if ((dataManager.getDraft().getTeams().get(i).getStartingPlayers().get(k).getPosition()).equals("U")) {
                        positionCount[9]++;
                    }
                }
                taxi = false;
                if (positionCount[0] < 2) {
                    pos.append("C");
                }
                else if (positionCount[1] < 1) {
                    pos.append("1B");
                }
                else if (positionCount[3] < 1) {
                    pos.append("3B");
                }
                else if (positionCount[2] < 1) {
                    pos.append("CI");
                }
                else if (positionCount[4] < 1) {
                    pos.append("2B");
                }
                else if (positionCount[6] < 1) {
                    pos.append("SS");
                }
                else if (positionCount[5] < 1) {
                    pos.append("MI");
                }
                else if (positionCount[7] < 5) {
                    pos.append("OF");
                }
                else if (positionCount[8] < 9) {
                    pos.append("P");
                }
                else if (positionCount[9] < 1) {
                    pos.append("U");
                }
                else {
                    taxi = true;
                    
                }
                if (taxi == true) {
                    Player newPlayer = dataManager.getDraft().getPlayers().get(0);
                    if ((newPlayer.getQP().length() == 1) || (newPlayer.getQP().length() == 2)) {
                        pos.append(newPlayer.getQP());
                    }
                    else if (newPlayer.getQP().length() > 2) {
                        if (newPlayer.getQP().charAt(1) == '_') {
                            pos.append(newPlayer.getQP().substring(0, 1));
                        }
                        else if (newPlayer.getQP().charAt(2) == '_') {
                            pos.append(newPlayer.getQP().substring(0, 2));
                        }
                    }
                    String position = pos.toString();
                    dataManager.getDraft().getPlayers().remove(newPlayer);
                    newPlayer.setSalary(1);
                    newPlayer.setContract("S2");
                    dataManager.getDraft().getTeams().get(i).addPlayer((newPlayer), position);
                    dataManager.getDraft().getDraftPicks().add(new DraftPick((dataManager.getDraft().getDraftPicks().size() + 1), (newPlayer), (dataManager.getDraft().getTeams().get(i))));
                }
                else if (taxi == false) {
                    String position = pos.toString();
                    for (j = 0;j < dataManager.getDraft().getPlayers().size();j++) {
                        if (position.equals("CI")) {
                            if ((dataManager.getDraft().getPlayers().get(j).getQP()).contains("1B") && (dataManager.getDraft().getPlayers().get(j).getQP()).contains("3B")) {
                                Player newPlayer = dataManager.getDraft().getPlayers().get(j);
                                dataManager.getDraft().getPlayers().remove(newPlayer);
                                newPlayer.setSalary(1);
                                newPlayer.setContract("S2");
                                dataManager.getDraft().getTeams().get(i).addPlayer((newPlayer), position);
                                dataManager.getDraft().getDraftPicks().add(new DraftPick((dataManager.getDraft().getDraftPicks().size() + 1), (newPlayer), (dataManager.getDraft().getTeams().get(i))));
                                break;
                            }   
                        }
                        else if (position.equals("MI")) {
                            if ((dataManager.getDraft().getPlayers().get(j).getQP()).contains("2B") && (dataManager.getDraft().getPlayers().get(j).getQP()).contains("SS")) {
                                Player newPlayer = dataManager.getDraft().getPlayers().get(j);
                                dataManager.getDraft().getPlayers().remove(newPlayer);
                                newPlayer.setSalary(1);
                                newPlayer.setContract("S2");
                                dataManager.getDraft().getTeams().get(i).addPlayer((newPlayer), position);
                                dataManager.getDraft().getDraftPicks().add(new DraftPick((dataManager.getDraft().getDraftPicks().size() + 1), (newPlayer), (dataManager.getDraft().getTeams().get(i))));
                                break;
                            }   
                        }
                        else if (position.equals("U")) {
                            if (!(dataManager.getDraft().getPlayers().get(j).getQP()).contains("P")) {
                                Player newPlayer = dataManager.getDraft().getPlayers().get(j);
                                dataManager.getDraft().getPlayers().remove(newPlayer);
                                newPlayer.setSalary(1);
                                newPlayer.setContract("S2");
                                dataManager.getDraft().getTeams().get(i).addPlayer((newPlayer), position);
                                dataManager.getDraft().getDraftPicks().add(new DraftPick((dataManager.getDraft().getDraftPicks().size() + 1), (newPlayer), (dataManager.getDraft().getTeams().get(i))));
                                break;
                            }  
                        }
                        else {
                            if ((dataManager.getDraft().getPlayers().get(j).getQP()).contains(position)) {
                                Player newPlayer = dataManager.getDraft().getPlayers().get(j);
                                dataManager.getDraft().getPlayers().remove(newPlayer);
                                newPlayer.setSalary(1);
                                newPlayer.setContract("S2");
                                dataManager.getDraft().getTeams().get(i).addPlayer((newPlayer), position);
                                dataManager.getDraft().getDraftPicks().add(new DraftPick((dataManager.getDraft().getDraftPicks().size() + 1), (newPlayer), (dataManager.getDraft().getTeams().get(i))));
                                break;
                            }
                        }
                    }
                }
                
            }
            else {
                //do nothing, no teams
            }
    }
    public void sortPlayers(String criteria) {
        
        if (criteria.equals("all")) {
            playersList.setItems(((dataManager.getDraft()).getPlayers()));
        }
        else if (criteria.equals("P")) {
            ObservableList<Player> newPlayers = FXCollections.observableArrayList();
            for (int i = 0;i < ((dataManager.getDraft()).getPlayers()).size();i++) {
                if ( ((dataManager.getDraft()).getPlayers()).get(i).getIsPitcher() ) {
                    newPlayers.add(((dataManager.getDraft()).getPlayers()).get(i));
                }
            }
            playersList.setItems(newPlayers);
        }
        else if (criteria.equals("CI")) {
            ObservableList<Player> newPlayers = FXCollections.observableArrayList();
            for (int i = 0;i < ((dataManager.getDraft()).getPlayers()).size();i++) {
                if (!(((dataManager.getDraft()).getPlayers()).get(i).getIsPitcher())) {
                    if (((((dataManager.getDraft()).getPlayers()).get(i)).getQP().contains("1B")) || ((((dataManager.getDraft()).getPlayers()).get(i)).getQP().contains("3B"))) {
                        newPlayers.add(((dataManager.getDraft()).getPlayers()).get(i));
                    }
                }
            }
            playersList.setItems(newPlayers);
        }
        else if (criteria.equals("MI")) {
            ObservableList<Player> newPlayers = FXCollections.observableArrayList();
            for (int i = 0;i < ((dataManager.getDraft()).getPlayers()).size();i++) {
                if (!(((dataManager.getDraft()).getPlayers()).get(i).getIsPitcher())) {
                    if (((((dataManager.getDraft()).getPlayers()).get(i)).getQP().contains("2B")) || ((((dataManager.getDraft()).getPlayers()).get(i)).getQP().contains("SS"))) {
                        newPlayers.add(((dataManager.getDraft()).getPlayers()).get(i));
                    }
                }
            }
            playersList.setItems(newPlayers);
        }
        else if (criteria.equals("U")) {
            ObservableList<Player> newPlayers = FXCollections.observableArrayList();
            for (int i = 0;i < ((dataManager.getDraft()).getPlayers()).size();i++) {
                if (!(((dataManager.getDraft()).getPlayers()).get(i).getIsPitcher())) {
                    newPlayers.add(((dataManager.getDraft()).getPlayers()).get(i));
                }
            }
            playersList.setItems(newPlayers);
        }
        else {
            ObservableList<Player> newPlayers = FXCollections.observableArrayList();
            for (int i = 0;i < ((dataManager.getDraft()).getPlayers()).size();i++) {
                if (!(((dataManager.getDraft()).getPlayers()).get(i).getIsPitcher())) {
                    if ( (((dataManager.getDraft()).getPlayers()).get(i)).getQP().contains(criteria) ) {
                        newPlayers.add(((dataManager.getDraft()).getPlayers()).get(i));
                    }
                }
            }
            playersList.setItems(newPlayers);
        }
        
    }
    
    private void sortTextField(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            ObservableList<Player> visiblelist = FXCollections.observableArrayList();
            if (newValue == null || newValue.isEmpty()) {
            }
            String userinput = newValue.toUpperCase();
            for (int i = 0; i < (playersList.getItems()).size(); i++) {
                // String fname = pData.get(i).getFirstname().toUpperCase();
                if ((playersList.getItems()).get(i).getFirstName().toUpperCase().startsWith(userinput.toUpperCase())) {  //user input equals; name equals input (swap)
                    visiblelist.add((playersList.getItems()).get(i));

                    // String lname = pData.get(i).getLastname().toUpperCase();
                } else if ((playersList.getItems()).get(i).getLastName().toUpperCase().startsWith(userinput.toUpperCase())) {
                    visiblelist.add((playersList.getItems()).get(i));

                } else {

                }

            }
            playersList.setItems(visiblelist);
        });
    }
    
    // REGISTER THE EVENT LISTENER FOR A TEXT FIELD
    private void registerTextFieldController(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            searchPlayers(textField.getText());
        });
    }
    
    private void searchPlayers(String text) {
        if (text.equals("") || text.equals(null)) {
            playersList.setItems(((dataManager.getDraft()).getPlayers()));
        }
        else {
            ObservableList<Player> newPlayers = FXCollections.observableArrayList();
            for (int i = 0;i < ((dataManager.getDraft()).getPlayers()).size();i++) {
                if (((dataManager.getDraft()).getPlayers()).get(i).getIsPitcher()) {
                    if ( ((Player)((dataManager.getDraft()).getPlayers()).get(i)).getFirstName().contains(text) || ((Player)((dataManager.getDraft()).getPlayers()).get(i)).getLastName().contains(text)) {
                        newPlayers.add(((dataManager.getDraft()).getPlayers()).get(i));
                    }
                }
                else {
                    if ( ((Player)((dataManager.getDraft()).getPlayers()).get(i)).getFirstName().contains(text) || ((Player)((dataManager.getDraft()).getPlayers()).get(i)).getLastName().contains(text) ) {
                        newPlayers.add(((dataManager.getDraft()).getPlayers()).get(i));
                    }
                }
            }
        }
    }
    
    //LOADS SCENE.
    /*
    0 = initial scene
    1 = fantasy teams
    2 = available players
    3 = fantasy standings
    4 = draft summary
    5 = MLB teams
    */
    private void loadScene(int scene) {
        if (scene == 0) {
            primaryStage.setScene(initialScene);
                primaryStage.show();
        }
        else if (scene == 1) {
            primaryStage.setScene(fantasyTeamsScene);
                primaryStage.show();
        }
        else if (scene == 2) {
            primaryStage.setScene(availablePlayersScene);
                primaryStage.show();
        }
        else if (scene == 3) {
            primaryStage.setScene(fantasyStandingsScene);
                primaryStage.show();
        }
        else if (scene == 4) {
            for (int i = 0;i < dataManager.getDraft().getTeams().size();i++) {
                dataManager.getDraft().getTeams().get(i).updateInfo();
            }
            dataManager.getDraft().setPoints();
            
            primaryStage.setScene(draftSummaryScene);
                primaryStage.show();
        }
        else if (scene == 5) {
            (dataManager.getDraft().getPlayers()).sort((Player o1, Player o2) -> o1.getLastName().compareTo(o2.getLastName()));
            primaryStage.setScene(MLBTeamsScene);
                primaryStage.show();
        }
        else {
            primaryStage.setScene(fantasyTeamsScene);
                primaryStage.show();
        }
    }
    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());
   
        initialScene = new Scene(initialBorderPane);
        fantasyTeamsScene = new Scene(fantasyTeamsBorderPane);
        availablePlayersScene = new Scene(availablePlayersBorderPane);
        fantasyStandingsScene = new Scene(fantasyStandingsBorderPane);
        draftSummaryScene = new Scene(draftSummaryBorderPane);
        MLBTeamsScene = new Scene(MLBTeamsBorderPane);
        
        initialScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        fantasyTeamsScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        availablePlayersScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        fantasyStandingsScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        draftSummaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        MLBTeamsScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        

        
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        loadScene(0);
    }


    @Override
    public void reloadDraft(Draft draftToReload) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
   
}
