/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wolfieballdraftkit;

/**
 *
 * @author Tyler
 */
public enum WBDK_PropertyType {
        // LOADED FROM properties.xml
        PROP_APP_TITLE,
        
        // APPLICATION ICONS
        NEW_DRAFT_ICON,
        LOAD_DRAFT_ICON,
        SAVE_DRAFT_ICON,
        EXPORT_PAGE_ICON,
        EXIT_ICON,
        
        FANTASY_TEAMS_ICON,
        AVAILABLE_PLAYERS_ICON,
        FANTASY_STANDINGS_ICON,
        DRAFT_SUMMARY_ICON,
        MLB_TEAMS_ICON,
        
        DELETE_ICON,
        ADD_ICON,
        MINUS_ICON,
        EDIT_ICON,
        MOVE_UP_ICON,
        MOVE_DOWN_ICON,
        
        STAR_BUTTON,
        START_BUTTON,
        STOP_BUTTON,
        
        // APPLICATION TOOLTIPS FOR BUTTONS
        NEW_DRAFT_TOOLTIP,
        LOAD_DRAFT_TOOLTIP,
        SAVE_DRAFT_TOOLTIP,
        EXPORT_PAGE_TOOLTIP,
        FANTASY_TEAMS_TOOLTIP,
        AVAILABLE_PLAYERS_TOOLTIP,
        FANTASY_STANDINGS_TOOLTIP,
        DRAFT_SUMMARY_TOOLTIP,
        MLB_TEAMS_TOOLTIP,
        DELETE_TOOLTIP,
        EXIT_TOOLTIP,
        ADD_PLAYER_TOOLTIP,
        REMOVE_PLAYER_TOOLTIP,
        ADD_TEAM_TOOLTIP,
        REMOVE_TEAM_TOOLTIP,
        EDIT_TEAM_TOOLTIP,
        
        STAR_TOOLTIP,
        START_TOOLTIP,
        STOP_TOOLTIP,

        // FOR AVAILABLE PLAYERS TABLE AND SEARCH BAR
        FANTASY_TEAMS_HEADING_LABEL,
        AVAILABLE_PLAYERS_HEADING_LABEL,
        STANDINGS_HEADING_LABEL,
        DRAFT_SUMMARY_HEADING_LABEL,
        MLB_TEAMS_HEADING_LABEL,
        FIRST_TABLE_LABEL,
        LAST_TABLE_LABEL,
        MLB_TEAM_TABLE_LABEL,
        POSITION_TABLE_LABEL,
        YOB_TABLE_LABEL,
        R_W_TABLE_LABEL,
        HR_SV_TABLE_LABEL,
        RBI_K_TABLE_LABEL,
        SB_ERA_TABLE_LABEL,
        BA_WHIP_TABLE_LABEL,
        ESTIMATED_VALUE_TABLE_LABEL,
        NOTES_TABLE_LABEL,
        SEARCH_TEXTFIELD_LABEL,
        
        // FOR AVAILABLE PLAYERS BUTTON LABELS
        ALL_BUTTON_LABEL, //ALL
        CATCHER_BUTTON_LABEL, //C
        PITCHER_BOTTON_LABEL, //P
        FIRST_BASE_BUTTON_LABEL, //1B
        SECOND_BASE_BUTTON_LABEL, //2B
        SHORTSTOP_BUTTON_LABEL, //SS
        THIRD_BASE_BUTTON_LABEL, //3B
        OUTFIELDER_BUTTON_LABEL, //OF
        CORNER_INFIELDER_BUTTON_LABEL, //CI
        MIDDLE_INFIELDER_BUTTON_LABEL, //MI
        UTILITY_INFIELDER_BUTTON_LABEL, //U
        
        // ERROR DIALOG MESSAGES
        START_DATE_AFTER_END_DATE_ERROR_MESSAGE,
        START_DATE_NOT_A_MONDAY_ERROR_MESSAGE,
        END_DATE_NOT_A_FRIDAY_ERROR_MESSAGE,
        ILLEGAL_DATE_MESSAGE,
        
        // AND VERIFICATION MESSAGES
        NEW_DRAFT_CREATED_MESSAGE,
        DRAFT_LOADED_MESSAGE,
        DRAFT_SAVED_MESSAGE,
        SITE_EXPORTED_MESSAGE,
        SAVE_UNSAVED_WORK_MESSAGE,
        REMOVE_PLAYER_MESSAGE,
        REMOVE_TEAM_MESSAGE
        
}
